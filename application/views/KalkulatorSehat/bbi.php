<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true) ?>
    <style>
        label {
            font-size: 16px;
            font-family: Arial, Helvetica, sans-serif;
            font-weight: 900;
        }

        .card {
            -webkit-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            -moz-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
        }
    </style>
</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>

    <div class="container-fluid mt-5">
        <div class="row pl-5 mt-4">
            <div class="col-4 pl-5">
                <p class="h3">Body Mass Index</p>
                <hr class="text-secondary">
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-8 mx-auto">
                <p class="h4 text-center mb-3 text-secondary">Isi data berikut</p>
                <div class="card pb-5">

                    <div class="card-body">
                        <form action="<?= site_url('kalkulatorsehat/hasilbbi') ?>" method="post">

                            <div class="form-group mt-5">
                                <div class="row">
                                    <div class="col-md-3 float-left">
                                        <label class="text-secondary" for="exampleInputEmail1 ">Tinggi Badan</label>
                                    </div>
                                    <div class="col-md-2 float-left">
                                        <input type="text" class="form-control float-left" value="" oninput="nilai2(value)" id="tinggi" name="tinggi" id="exampleInputEmail1" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-md-6 float-left pt-3">
                                        <input type="range" min="100" max="200" value="100" id="vol" oninput="nilai(value)" class="form-control-range" id="formControlRange">
                                    </div>

                                </div>
                                <div class="row mt-3">
                                    <div class="col-md-3 float-left">
                                        <label class="text-secondary" for="exampleInputEmail1 ">Berat Badan</label>
                                    </div>
                                    <div class="col-md-2 float-left">
                                        <input type="text" class="form-control float-left" value="" name="berat" id="berat" oninput="nilai3(value)" aria-describedby="emailHelp">
                                    </div>
                                    <div class="col-md-6 float-left pt-2">
                                        <input type="range" min="0" max="130" value="0" id="berat2" oninput="nila4(value)" class="form-control-range" id="formControlRange">
                                    </div>
                                </div>
                                <div class="row mt-5">
                                    <div class="col-md-3 mx-auto">
                                        <input type="submit" class="btn btn-primary px-5" name="hasil" value="Hasil" id="">
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <?= $this->load->view('_partials/footer', "", true) ?>
    <script>
        function nilai2(vol2) {
            document.querySelector('#vol').value = vol2;
        }

        function nilai(vol) {
            document.querySelector('#tinggi').value = vol;
        }

        function nilai3(vol3) {
            document.querySelector('#berat2').value = vol3;
        }

        function nila4(vol4) {
            document.querySelector('#berat').value = vol4;
        }
    </script>

    <?= $this->load->view('_partials/javascript', "", true) ?>

</body>

</html>