<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true) ?>
    <style>
        .corona-data {
            height: 116px;
            width: 260px;
            border-radius: 5px;
            box-shadow: 0 5px 10px rgba(19, 191, 166, 0.3) !important;
        }
    </style>
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.css">
    <link rel="stylesheet" href="<?= base_url("assets/table.css") ?>">
</head>

<body id="tampil">
    <?= $this->load->view('_partials/navbar', "", true) ?>

    <div class="container-fluid">
        <div class="row text-white" style="margin-top:114px; background-color: #1a9dff;">
            <div class="col-md-6 pl-5 pt-5" style="font-family: Arial, sans-serif;">
                <p class="display-4">Statistik Penyebaran Virus Corona</p>
                <p style="line-height: 30px;">Dapatkan informasi mengenai Live Data penyebaran Virus Corona di Indonesia</p>
            </div>
            <div class="col-md-6">
                <img src="<?= base_url('assets/image/statistic.png') ?>" class="img-fluid" width="520px" alt="">
            </div>
        </div>
    </div>
    <div class="container mt-5">
        <div class="row">
            <div class="col lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card corona-data" style="background-color: #d43f8d;">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="text-white">
                                <p class="mb-0">TOTAL POSITIF</p>
                                <?php
                                $tpositif = file_get_contents('https://api.kawalcorona.com/indonesia');
                                $positif = json_decode($tpositif, true);
                                foreach ($positif as $item) :

                                ?>
                                    <p class="text-white "><?= $item['positif']; ?></p>
                                <?php endforeach; ?>
                                <p class="mb-2">Orang</p>

                            </div>
                            <div class="ml-auto">
                                <img src="<?= base_url('assets/icon/sad.png') ?>" width="60px" alt="sad">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card corona-data" style="background-color:#f82649 ">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="text-white">
                                <p class="mb-0">TOTAL SEMBUH</p>
                                <?php
                                foreach ($positif as $item) :

                                ?>
                                    <p class="text-white "><?= $item['sembuh']; ?></p>
                                <?php endforeach; ?>
                                <p class="mb-2">Orang</p>

                            </div>
                            <div class="ml-auto">
                                <img src="<?= base_url('assets/icon/happy.png') ?>" width="60px" alt="sad">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col lg-4 col-md-4 col-sm-12 col-xs-12">
                <div class="card corona-data" style="background-color: #09ad95;">
                    <div class="card-body">
                        <div class="d-flex">
                            <div class="text-white">
                                <p class="mb-0">TOTAL MENINGGAL</p>
                                <?php
                                foreach ($positif as $item) :

                                ?>
                                    <p class="text-white "><?= $item['meninggal']; ?></p>
                                <?php endforeach; ?>
                                <p class="mb-2">Orang</p>

                            </div>
                            <div class="ml-auto">
                                <img src="<?= base_url('assets/icon/cry.png') ?>" width="60px" alt="sad">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row mt-5">
            <div class="col-lg-12 col-md-12 ">
                <div id="statistik" style="display: block;
	margin-left: auto;
	margin-right: auto;
	width: 70%;">

                </div>
                <div class="labels mt-1">
                    <p class="text-center text-secondary" style="font-size: 12px;"> Jumlah Data dalam ribuan</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12 col-md-12">
                <div class="table-users ">
                    <div class="header">Data Kasus Virus Corona Provinsi</div>

                    <table cellspacing="0">
                        <tr>
                            <th class="text-center">Provinsi</th>
                            <th class="text-center">Positif</th>
                            <th class="text-center">Sembuh</th>
                            <th class="text-center">Meninggal</th>
                        </tr>
                        <?php foreach ($provinsi as $pvc) : ?>
                            <tr>
                                <td class="text-center"><?= $pvc["attributes"]["Provinsi"] ?></td>
                                <td class="text-center"><?= $pvc["attributes"]["Kasus_Posi"] ?> </td>
                                <td class="text-center"><?= $pvc["attributes"]["Kasus_Semb"] ?></td>
                                <td class="text-center"><?= $pvc["attributes"]["Kasus_Meni"] ?></td>
                            </tr>
                        <?php endforeach; ?>
                    </table>
                </div>
            </div>
        </div>
    </div>

    <?= $this->load->view('_partials/footer', "", true) ?>
    <?= $this->load->view('_partials/javascript', "", true) ?>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.0/jquery.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/morris.js/0.5.1/morris.min.js"></script>
    <script>
        var serries = JSON.parse(`<?php echo $coba; ?>`);

        new Morris.Line({
            // ID of the element in which to draw the chart.
            element: 'statistik',
            // Chart data records -- each entry in this array corresponds to a point on
            // the chart.
            data: [{
                    keterangan: 'Positif',
                    orang: serries[0]["positif"]
                },
                {
                    keterangan: 'Dirawat',
                    orang: serries[0]["dirawat"]
                },

                {
                    keterangan: 'Sembuh',
                    orang: serries[0]["sembuh"]
                },
                {
                    keterangan: 'Meninggal',
                    orang: serries[0]["meninggal"]
                },

            ],
            parseTime: false,
            // The name of the data record attribute that contains x-values.
            xkey: ['keterangan'],
            // A list of names of data record attributes that contain y-values.
            ykeys: ['orang'],
            // Labels for the ykeys -- will be displayed when you hover over the
            // chart.
            labels: ['orang'],

        });
    </script>


</body>

</html>