<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true) ?>
    <style>
        .rainbow {
            -webkit-appearance: none;
            width: 820px;
            height: 10px;
        }

        .rainbow::-webkit-slider-runnable-track {
            width: 100%;
            height: 25.8px;
            cursor: pointer;
            box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
            background: linear-gradient(to right,
                    #3495eb 20%,
                    green 40%,
                    yellow 60%,
                    orange 80%,
                    red 100%);
            border-radius: 25px;
            border: 0px solid #000101;
        }

        .rainbow::-webkit-slider-thumb {
            background-color: black !important;
            height: 25px;
            width: 14px;
            border-radius: 7px;
            cursor: pointer;
            -webkit-appearance: none;
            margin-top: -5.5px;
            box-shadow: 2px 2px 2px rgba(0, 0, 0, .5);
        }
    </style>
</head>


<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>
    <div class="container">
        <div class="row mt-5">
            <div class="col-sm-1 pt-4">
                <img class="img-fluid" src="<?= base_url('assets/icon/weight2.svg') ?>" width="57px" alt="">
            </div>
            <div class="col-sm-11 pt-4">
                <p class="h4 mt-2">Kalkulator Body Mass Index</p>
            </div>
        </div>
        <hr>
        <div class="card">
            <div class="card-body">
                <div class="row">
                    <div class="col-sm-12">
                        <p class="h4 text-primary mb-2">Hasil Perhitungan</p>
                        <p>Berat dan tinggi anda adalah <?= $tinggi ?> m dan <?= $berat ?> kg </p>
                        <?php $angka = number_format($hasil, 2) ?>
                        <p class="font-weight-bold">Body Mass Index kamu adalah <?= $angka; ?></p>
                    </div>
                </div>
                <div class="row mb-3">
                    <input class="rainbow mx-auto mt-3" min="0" max="42" value="<?= $angka ?>" type="range" min="0" max="360" disabled />
                </div>
                <div class="row">
                    <div class="col-sm-12 ">
                        <?php if ($angka <= 18.5) { ?>
                            <h3 class="text-center">Your BMI is Berat Badan Kurang</h3>
                            <p>Anda termasuk kurus atau memiliki berat badan kurang, jika angka BMI Anda berada di bawah 18,5. Bagaimana cara menambah berat badan? Jika Anda ingin menaikkan berat badan, Anda perlu mengonsumsi makanan dan minuman dengan jumlah kalori yang lebih besar dari kebutuhan kalori harian Anda. Anda bisa menambahkan asupan kalori Anda sebanyak 300-500 kkal per hari. Misalnya, jika kebutuhan total kalori harian Anda adalah 1700 kkal, Anda perlu mengonsumsi makanan dengan total 1700+500 = 2200 kkal per hari. </p>
                        <?php } elseif ($angka >= 18.5 && $angka <= 24.9) { ?>
                            <h3 class="text-center">Your BMI is Berat badan ideal</h3>
                            <p>Berat badan Anda bisa dikatakan ideal jika angka BMI Anda berada antara angka 18,5 sampai 22,9. Bagaimana cara menjaga agar berat badan tetap ideal? Anda perlu mengonsumsi makanan dan minuman sesuai dengan kebutuhan kalori harian Anda, untuk mempertahankan berat badan ideal seperti sekarang. Misalnya, jika kebutuhan kalori harian Anda adalah 1950 kkal, maka Anda harus mengonsumsi makanan dengan total kalori 1950 per harinya.</p>
                        <?php } elseif ($angka >= 25 && $angka <= 29.9) { ?>
                            <h3 class="text-center">Your BMI is Berlebih</h3>
                            <p>Untuk menurunkan berat badan, Anda harus mengonsumsi makanan dan minuman dengan jumlah kalori yang lebih kecil dari kebutuhan kalori harian Anda. Kurangi sebanyak 300-500 kalori per hari dari hasil perhitungan yang Anda dapatkan. Mengurangi jumlah asupan Anda setidaknya 500 kkal per hari dapat membantu menurunkan berat badan Anda sebesar 0,5-1 kg per minggu. Misalnya, jika hasil dari perhitungan menunjukkan bahwa kebutuhan total kalori harian Anda adalah 2100 kkal, maka Anda hanya perlu mengonsumsi asupan kalori sebesar 1600 kkal per hari (2100-500 kkal).</p>
                        <?php } elseif ($angka >= 30) { ?>
                            <h3 class="text-center">Your BMI is Obesitas</h3>
                            <p>Untuk menurunkan berat badan, Anda harus mengonsumsi makanan dan minuman dengan jumlah kalori yang lebih kecil dari kebutuhan kalori harian Anda. Kurangi sebanyak 300-500 kalori per hari dari hasil perhitungan yang Anda dapatkan. Mengurangi jumlah asupan Anda setidaknya 500 kkal per hari dapat membantu menurunkan berat badan Anda sebesar 0,5-1 kg per minggu. Misalnya, jika hasil dari perhitungan menunjukkan bahwa kebutuhan total kalori harian Anda adalah 2100 kkal, maka Anda hanya perlu mengonsumsi asupan kalori sebesar 1600 kkal per hari (2100-500 kkal). </p>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>

    </div>




    <?= $this->load->view('_partials/footer', "", true) ?>
    <?= $this->load->view('_partials/javascript', "", true) ?>
</body>


</html>