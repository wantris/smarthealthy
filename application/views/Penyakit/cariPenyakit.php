<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", True) ?>
</head>


<body>
    <?= $this->load->view('_partials/navbar', "", True) ?>

    <div class="list-head mt-5">
        <div class="container-fluid pt-3 pb-3" style="background-color: #1a9dff">
            <div class="row pl-5 pr-5">
                <div class="col-sm-6">
                    <p class="h2 text-white">Penyakit</p>
                    <p class="h5 text-white">Cari nama penyakit</p>
                    <form action="<?php site_url('penyakit/search') ?>" method="post">
                        <input type="text" class="form-control" name="keyword" style="width: 250px;">
                        <input type="submit" class="btn btn-light text-primary mt-3 pr-3 pl-3" value="Cari">
                    </form>
                </div>
                <div class="col-sm-6">
                    <img src="<?= base_url('assets/image/penyakit.png') ?>" class="mx-auto d-block" style="margin-right: 100px;" width="300px" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="list-body">
        <div class="container">
            <ul class="row">
                <?php foreach ($penyakits as $penyakit) : ?>
                    <li class="col-sm-4 mt-3" style="list-style-type: none;">
                        <a href="#" class="text-secondary" style="text-decoration: none; font-family:Arial, Helvetica, sans-serif"><?= $penyakit->nama_penyakit ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <?= $this->load->view('_partials/footer', "", True) ?>
    <?= $this->load->view('_partials/javascript', "", True) ?>
</body>

</html>