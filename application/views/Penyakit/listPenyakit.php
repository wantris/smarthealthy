<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", True) ?>
</head>


<body>
    <?= $this->load->view('_partials/navbar', "", True) ?>

    <div class="list-head">
        <div class="container-fluid pt-3 pb-3" style="background-color: #1a9dff; margin-top: 120px">
            <div class="row pl-5 pr-5">
                <div class="col-sm-6">
                    <p class="h2 text-white mt-5">Penyakit</p>
                    <p class="h5 text-white">Cari nama penyakit</p>
                    <form action="<?= site_url('penyakit/search') ?>" method="GET">
                        <input type="text" class="form-control" name="keyword" style="width: 250px;" autocomplete="off">
                        <input type="submit" class="btn btn-light text-primary mt-3 pr-3 pl-3" value="Cari">
                    </form>
                </div>
                <div class="col-sm-6">
                    <img src="<?= base_url('assets/image/penyakit.png') ?>" class="mx-auto d-block" style="margin-right: 100px;" width="300px" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="list-body ">
        <div class="container-fluid">
            <ul class="row ml-4 mr-4 mt-4">
                <?php foreach ($penyakit->result() as $penyakit) : ?>
                    <li class="col-sm-3 mt-3" style="list-style-type: none;">
                        <a href="<?= site_url('penyakit/getPenyakit/' . $penyakit->id_penyakit) ?>" class="text-secondary" style="text-decoration: none; font-family:'Poppins', sans-serif"><?= $penyakit->nama_penyakit ?></a>
                    </li>
                <?php endforeach; ?>
            </ul>
        </div>
    </div>
    <?= $this->load->view('_partials/footer', "", True) ?>
    <?= $this->load->view('_partials/javascript', "", True) ?>
</body>

</html>