<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <style>
        a {
            text-decoration: none;
            color: black;
            font-family: Arial, sans-serif;
            font-size: 18px;
        }

        .card {
            -webkit-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            -moz-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
        }

        .crimson {
            font-family: 'Crimson Text', serif;
        }
    </style>
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Crimson+Text&display=swap" rel="stylesheet">
</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>

    <!-- Poster -->
    <div class="container-fluid">
        <div class="row mt-5 pl-5 ">
            <div class="col-md-8 ">
                <?php foreach ($penyakit as $item) : ?>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <h2 class="float-left text-secondary" style="font-size: 30px;letter-spacing:0.25px;font-weight:400"><?= $item->nama_penyakit ?></h2>
                        </div>
                    </div>
                    <div class="row mt-5 pr-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="text-secondary crimson" style="font-size: 20px;font-weight:400;letter-spacing:0.25px">Deskripsi</p>
                            <hr class="text-secondary">
                            <div class="deskripsi mt-2">
                                <?= $item->deskripsi ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 pr-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="text-secondary crimson" style="font-size: 20px;font-weight:400;letter-spacing:0.25px">Pencegahan</p>
                            <hr class="text-secondary">
                            <div class="pencegahan mt-2">
                                <?= $item->pencegahan ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 pr-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="text-secondary crimson" style="font-size: 20px;font-weight:400;letter-spacing:0.25px">Gejala</p>
                            <hr class="text-secondary">
                            <div class="gejala mt-2">
                                <?= $item->gejala ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 pr-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="text-secondary crimson" style="font-size: 20px;font-weight:400;letter-spacing:0.25px">Penyebab</p>
                            <hr class="text-secondary">
                            <div class="penyebab mt-2">
                                <?= $item->penyebab ?>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-5 pr-3">
                        <div class="col-lg-12 col-md-12 col-sm-12">
                            <p class="text-secondary crimson" style="font-size: 20px;font-weight:400;letter-spacing:0.25px">Penanganan</p>
                            <hr class="text-secondary">
                            <div class="penanganan mt-2">
                                <?= $item->penanganan ?>
                            </div>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
            <div class="col-md-4">
                <aside class="float-left" style="margin-top:100px; width:370px; position: -webkit-sticky;
  position: sticky; ">
                    <div class="row px-3 py-3">
                        <div class="card" style="width: 300px;">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/eye-makeup.svg') ?>" class="mr-3" width="40px" alt=""> Kecantikan</a>
                            </div>
                        </div>
                    </div>
                    <div class="row px-3 py-3">
                        <div class="card" style="width:300px">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/gym.svg') ?>" class="mr-3" width="35px" alt="">Olahraga</a>
                            </div>
                        </div>
                    </div>
                    <div class="row px-3 py-3">
                        <div class="card" style="width:300px">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/eye.svg') ?>" class="mr-4" width="35px" alt="">Mata</a>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <?= $this->load->view('_partials/footer', "", true) ?>
    <?= $this->load->view('_partials/javascript', "", true) ?>
</body>

</html>