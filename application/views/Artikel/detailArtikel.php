<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta content="width=device-width, initial-scale=1" name="viewport" />
    <?= $this->load->view('_partials/head', "", true); ?>
    <style>
        #banner img {
            width: 1100px;
            height: 440px;
        }

        @media only screen and (max-width: 600px) {
            #banner img {
                width: 300px;
                height: 220px;
            }

            .boxstory img {
                width: 600px;
                height: 220px;
            }
        }

        #judul h1 {
            font-size: 40px;
            letter-spacing: 0.25px;
        }

        #neon3 {
            padding-left: 25px !important;
            padding-right: 25px !important;
            font-size: 11px !important;
        }

        #detailKonten {
            font-family: 'Crimson Text', Georgia, serif !important;
            padding-right: 80px;
            letter-spacing: 0.25px;
            line-height: 30px;
        }
    </style>

</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true); ?>
    <?php foreach ($detail as $item) : ?>
        <div class="container" style="margin-top: 100px;">
            <div class="row">
                <div class="col-sm-12">
                    <span id="banner"><img src="<?= base_url('upload/poster/' . $item->foto) ?>" alt="Responsive image"></span>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-12 col-md-3 col-lg-3" style="padding-top: 60px;">
                    <div class="row">
                        <div class="col-sm-4">
                            <img src="<?= base_url('upload/profil/' . $item->photo) ?>" style="width: 90px; border-radius:50%;" class="img-fluid" alt="">
                        </div>
                        <div class="col-sm-8">
                            <a href="" class="text-primary" style="font-weight: 400;text-decoration:none; font-weight:700; font-size:14px;letter-spacing:0.25px"><?= $item->nama_depan . " " . $item->nama_belakang ?></a>
                            <div>
                                <p class="text-secondary" style="font-size: 11px;"><?php echo format_indo(date('Y-m-d', strtotime($item->tanggal))); ?></p>
                            </div>

                        </div>
                    </div>
                    <hr class="text-secondary">
                    <div class="row mt-4">
                        <div class="col-sm-12">
                            <a href="">
                                <div class="facebook float-left">
                                    <span class="fa fa-facebook"></span>
                                </div>
                                <div class="float-left ml-3 mt-2 text-secondary" id="caption">
                                    Bagikan Ke facebook
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12">
                            <a href="">
                                <div class="twitter float-left">
                                    <span class="fa fa-twitter"></span>
                                </div>
                                <div class="float-left ml-3 mt-2 text-secondary" id="caption">
                                    Bagikan Ke Twitter
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12">
                            <a href="">
                                <div class="whatsapp float-left">
                                    <span class="fa fa-whatsapp"></span>
                                </div>
                                <div class="float-left ml-3 mt-2 text-secondary" id="caption">
                                    Bagikan Ke Whatsapp
                                </div>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-sm-12 col-md-9 col-lg-9" style="padding-top: 60px;padding-bottom:100px">
                    <div id="kategori" class="mb-2">
                        <a href="" id="neon3" style="letter-spacing: 0.25px;text-transform:uppercase;"><?= $item->kategori ?></a>
                    </div>
                    <div id="judul" class="pr-3">
                        <h1><?= $item->judul ?></h1>
                    </div>
                    <div id="detailKonten" class="mt-4">
                        <?= $item->konten ?>
                    </div>
                    <!--  -->
                    <!-- <a href="http://www.facebook.com/sharer.php?u=https://smarthealty.com/index.php/artikel/detail/" target="_blank">
                        <img src="https://www.kursuswebsite.org/wp-content/uploads/2017/03/facebook.png" alt="Facebook" />
                    </a> -->
                </div>

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <p style="letter-spacing: 0.25;font-size:20px;">REKOMENDASI ARTIKEL</p>
                    <hr class="text-secondary">
                </div>
            </div>
            <div class="row">
                <?php
                $this->db->select('*');
                $this->db->from('artikel');
                $this->db->join('user', 'user.user_id = artikel.user_id');
                $this->db->order_by('rand()');
                $this->db->limit(2);
                $query = $this->db->get()->result();;
                foreach ($query as $query) :
                ?>
                    <div class="col-lg-4 sol-sm-12" id="box">
                        <div class="boxstory" style="width: 275px;height:517px;">
                            <div class="boxstory-image">
                                <img src="<?= base_url('upload/poster/' . $query->foto) ?>" style="width:275px; height:255px;" alt="">
                            </div>
                            <div class="boxstory-button mt-3">
                                <a href="" id="neon3" style="letter-spacing: 0.25px;text-transform:uppercase;"><?= $query->kategori ?></a>
                            </div>
                            <div class="boxstory-title mt-3" style="height:68px">
                                <p style="text-transform: uppercase;letter-spacing:0.25px;font-size:17px;font-weight:200"><?= $query->judul ?></p>
                            </div>
                            <div class="boxstory-content " style="height:80px;font-size:14px;">
                                <?= word_limiter($query->konten, 15); ?>
                            </div>
                            <div class="row mt-3">
                                <div class="col-sm-2">
                                    <img src="<?= base_url('upload/profil/' . $query->photo) ?>" style="width: 90px; border-radius:50%;" class="img-fluid" alt="">
                                </div>
                                <div class="col-sm-10">
                                    <a href="" class="text-primary" style="font-weight: 400;text-decoration:none; font-weight:700; font-size:14px;letter-spacing:0.25px"><?= $query->nama_depan . " " . $query->nama_belakang ?></a>
                                    <div id="konten">
                                        <p class="text-secondary"><?php echo format_indo(date('Y-m-d', strtotime($query->tanggal))); ?></p>
                                    </div>

                                </div>
                            </div>

                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>


    <?= $this->load->view('./_partials/footer.php', "", TRUE) ?>



    <?= $this->load->view('_partials/javascript', "", true); ?>
</body>

</html>