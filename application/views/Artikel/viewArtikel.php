<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true); ?>
    <style>
        a {
            text-decoration: none;
            color: black;
            font-family: 'Poppins', Arial, sans-serif;
        }

        #konten p {
            font-family: 'Crimson Text', serif !important;
            font-size: 17px;
            font-weight: 550;
        }
    </style>
    
</head>

<body>

    <?= $this->load->view('_partials/navbar', "", true); ?>



    <div class="container-fluid">
        <div class="row mt-5">
            <div class="col-md-8">
                <h3 class="text-center mb-5">Artikel Kesehatan</h2>
                    <!--Fetch data dari database-->

                    <?php if ($data->num_rows() == 0) {
                    } else {
                        foreach ($data->result() as $row) : ?>
                            <div class="row mb-3">
                                <div class="col-md-5 pl-5"><img src="<?= base_url('upload/poster/' . $row->foto) ?>" class="img-fluid img-thumbnail" style="width: 320px; height:220px;" alt=""></div>
                                <div class="col-md-7 pl-5" style="padding-top:10px">
                                    <a href="" id="neon3" style="letter-spacing: 0.25px;"><?= $row->kategori ?></a>
                                    <?php $judul = str_replace(" ", "-", $row->judul); ?>
                                    <div class="mt-2"><a style="letter-spacing: 0.25px; font-size:20px; text-decoration:none" href="<?= site_url('artikel/detail/' . $judul) ?>"><?php
                                                                                                                                                                                    $judul2 = str_replace("-", " ", $judul);
                                                                                                                                                                                    echo $judul2; ?></a></div>
                                    <div class="text-secondary" id="konten"><?= word_limiter($row->konten, 20) ?></div>
                                    <div class="row">
                                        <div class="col-sm-2">
                                            <img src="<?= base_url('upload/profil/' . $row->photo) ?>" style="width: 90px; border-radius:50%;" class="img-fluid" alt="">
                                        </div>
                                        <div class="col-sm-10">
                                            <a href="" class="text-primary" style="font-weight: 400;text-decoration:none; font-weight:700; font-size:14px;letter-spacing:0.25px"><?= $row->nama_depan . " " . $row->nama_belakang ?></a>
                                            <div id="konten">
                                                <p class="text-secondary"><?php echo format_indo(date('Y-m-d', strtotime($row->tanggal))); ?></p>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php endforeach; ?>
                        <p class="te"><?php echo $pagination;
                                    } ?></p>

            </div>
            <div class="col-md-4">
                <aside class="" style="margin-top:100px; width:370px; position: -webkit-sticky;
  position: sticky; ">
                    <div class="row px-3 py-3" style="border-bottom: 3px solid #f8f8f8; width:300px ">
                        <a href="#" class="text-secondary" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/calories.svg') ?>" width="23px" alt=""> KEBUTUHAN KALORI BASAL</a>
                    </div>
                    <div class="row px-3 py-3" style="border-bottom: 3px solid #f8f8f8; width:300px">
                        <a href="#" class="text-secondary" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/weight.svg') ?>" width="23px" alt=""> BERAT BADAN IDEAL</a>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <?= $this->load->view('./_partials/footer.php', "", TRUE) ?>



    <?= $this->load->view('_partials/javascript', "", true); ?>

</body>

</html>