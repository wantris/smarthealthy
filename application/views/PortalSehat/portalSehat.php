<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true); ?>
    <style>
        #pusat {
            box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        }

        a {
            text-decoration: none !important;
            color: black;
            font-family: 'poppins', sans-serif;
            font-size: 14px;
        }

        /* .card {
            width: 260px;
        } */
    </style>
</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true); ?>
    <div class="container-fluid">
        <div class="row text-white" style="margin-top:114px; background-color: #1a9dff;">
            <div class="col-md-6 pl-5 pt-5" style="font-family: Arial, sans-serif;">
                <p class="display-4">Portal Kesehatan</p>
                <p style="line-height: 30px;">Dapatkan kumpulan informasi, artikel, direktori, dan tools seputar topik kesehatan tertentu di fitur Portal Kesehatan! Sobat Sehat hanya perlu mengklik satu keategori kemudian beragam informasi bisa Kamu nikmati!</p>
            </div>
            <div class="col-md-6">
                <img src="<?= base_url('assets/image/doctor.png') ?>" class="img-fluid" width="520px" alt="">
            </div>
        </div>

        <!-- ROW1 -->
        <div class="row mt-5 mb-4 pl-4 pr-4">
            <div class="col-3">
                <p class="h4">Portal Kesehatan</p>
            </div>
        </div>
        <div class="row pl-4 pr-4">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalSehat/alergi') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/virus.svg') ?>" class="mr-3 img-fluid" width="40px" alt="">Alergi
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/kanker') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/cell.svg') ?>" class="mr-3 img-fluid" width="40px" alt="">Kanker
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/nutrisi') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/nutrition.svg') ?>" class="mr-3 img-fluid" width="40px" alt="">Nutrisi
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/mata') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/eye.svg') ?>" class="mr-3 img-fluid" width="40px" alt="">Mata
                        </div>
                    </div>
                </a>
            </div>
        </div>

        <!-- ROW2 -->
        <div class="row pl-4 pr-4 pt-5 pb-5">
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/olahraga') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/gym.svg') ?>" class="mr-3" width="40px" alt="">Olahraga
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/kecantikan') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/eye-makeup.svg') ?>" class="mr-3" width="40px" alt="">Kecantikan
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/pencernaan') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/intestine.svg') ?>" class="mr-3" width="40px" alt="">Pencernaan
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <a href="<?= site_url('portalsehat/mental') ?>">
                    <div class="card" id="pusat">
                        <div class=" card-body">
                            <img src="<?= base_url('assets/icon/trauma.svg') ?>" class="mr-3" width="40px" alt="">Mental
                        </div>
                    </div>
                </a>
            </div>
        </div>

    </div>
    <?= $this->load->view('_partials/footer', "", true); ?>
    <?= $this->load->view('_partials/javascript', "", true); ?>
</body>

</html>