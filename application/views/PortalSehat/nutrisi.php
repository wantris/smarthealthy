<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <style>
        a {
            text-decoration: none;
            color: black;
            font-family: Arial, sans-serif;
            font-size: 18px;
        }

        .card {
            -webkit-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            -moz-box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
            box-shadow: -2px 12px 17px 0px rgba(0, 0, 0, 0.46);
        }
    </style>
</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>

    <!-- Poster -->
    <div class="container-fluid">
        <div class="row text-white py-5" style="margin-top:114px; background-color: #1a9dff;">
            <div class="col-md-6 pl-5 pt-3" style="font-family: Arial, sans-serif;">
                <p class="display-4">Nutrisi</p>
                <p style="line-height: 30px;">Nutrisi adalah suatu subtansi organic yang ada dan di butuhkan oleh organisme yang memiliki manfaat menormalkan sistem tubuh, pertumbuhan tubuh dan juga sebagai pemeliharaan kesehatan.</p>
            </div>
            <div class="col-md-6" style="padding-left:130px">
                <img src="<?= base_url('assets/icon/nutrition2.svg') ?>" style="opacity: 80%;" class="" width="220px" alt="">
            </div>
        </div>
        <div class="row mt-5 pl-5">
            <div class="col-md-8">
                <!--Fetch data dari database-->

                <?php if ($data->num_rows() == 0) {
                } else {
                    foreach ($data->result() as $row) : ?>
                        <div class="row">
                            <div class="col-md-4"><?php echo $row->judul; ?></div>
                            <div class="col-md-7"><?php echo $row->konten; ?></div>
                        </div>
                    <?php endforeach; ?>
                    <p class="te"><?php echo $pagination;
                                } ?></p>

            </div>
            <div class="col-md-4">
                <aside class="float-left" style="margin-top:100px; width:370px; position: -webkit-sticky;
  position: sticky; ">
                    <div class="row px-3 py-3">
                        <div class="card" style="width: 300px;">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/cell.svg') ?>" class="mr-3" width="40px" alt="">Kanker</a>
                            </div>
                        </div>
                    </div>
                    <div class="row px-3 py-3">
                        <div class="card" style="width:300px">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/trauma.svg') ?>" class="mr-3" width="35px" alt="">Mental</a>
                            </div>
                        </div>
                    </div>
                    <div class="row px-3 py-3">
                        <div class="card" style="width:300px">
                            <div class="card-body">
                                <a href="#" class="text-secondary font-weight-bold" style="font-family:Arial, sans-serif; text-decoration: none; font-size:14px;"><img src="<?= base_url('assets/icon/intestine.svg') ?>" class="mr-4" width="35px" alt="">Pencernaan</a>
                            </div>
                        </div>
                    </div>
                </aside>
            </div>
        </div>
    </div>

    <?= $this->load->view('_partials/footer', "", true) ?>
    <?= $this->load->view('_partials/javascript', "", true) ?>
</body>

</html>