<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>

</head>

<body>
    <header>
        <!-- navbar-->
        <?= $this->load->view('./_partials/navbar.php', "", TRUE) ?>

        <!-- Jumbotron -->
        <div class="jumbotron" style="
          background-image: url(assets/image/bg-jumbo.jpg);
          background-size: 100% 170%;
          height: 500px;
        ">
            <h1 class="display-4 text-center font-weight-bold" style="font-family: madefor">
                Welcome To <strong style="color: #bf6565">Smart Healthy</strong>
            </h1>
            <div class="row">
                <a class="btn btn-primary btn-lg mx-auto mt-5" href="#" role="button" style="border-radius: 25px">Learn more</a>
            </div>
        </div>
    </header>

    <!-- Content -->
    <main class="mt-n3">
        <div class="container-fluid" id="konten">
            <div class="row justify-content-center pb-5 pl-5" style="background-color: #f2fbff;">
                <div class="col-md-3 mt-5">
                    <div class="card px-3 py-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); width:300px;">
                        <a href="" style="text-decoration: none;font-size:20px;" class="text-dark"><img src="<?= base_url('assets/icon/sehat.svg'); ?>" class="mr-3" width="60px" alt=""> <strong>Pusat Kesehatan</strong> </a>
                    </div>
                </div>
                <div class="col-md-3 mt-5">
                    <div class="card px-3 py-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); width:300px;">
                        <a href="" style="text-decoration: none;font-size:20px;" class="text-dark"><img src="<?= base_url('assets/icon/event.svg'); ?>" class="mr-3" width="60px" alt=""> <strong>Event</strong> </a>
                    </div>
                </div>
                <div class="col-md-3 mt-5">
                    <div class="card px-3 py-3" style="box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2); width:300px;">
                        <a href="" style="text-decoration: none;font-size:20px;" class="text-dark"><img src="<?= base_url('assets/icon/drugs.svg'); ?>" class="mr-3" width="60px" alt=""> <strong>Info Obat</strong> </a>
                    </div>
                </div>
            </div>
            <div class="row pt-5 pb-4">
                <div class="col-md-5">
                    <img src="<?= base_url('assets/image/heartDay.jpg') ?>" class="img-fluid" alt="" width="380px" />
                </div>
                <div class="col-md-6">
                    <p class="display-4 font-weight-bold">Happy World Heart Day</p>
                    <p class="mt-4">
                        Start from scratch or choose from over 500 designer-made templates
                        to make your own website. With the world’s most innovative drag
                        and drop website builder, you can customize or change anything.
                        Make your site come to life with video backgrounds, scroll effects
                        and animation. With the Wix Editor, you can create your own
                        professional website that looks stunning.
                    </p>
                    <a href="" class="text-dark"><img src="<?= base_url('assets/icon/arrow.svg') ?>" width="12px" style="transform: rotate(180deg)" /><strong> Get Started</strong>
                        <img src="<?= base_url('assets/icon/arrow.svg') ?>" width="12px" alt="" />
                    </a>
                </div>
            </div>
            <hr style="box-shadow: 0 4px 2px -2px rgba(0, 0, 0, 0.2)" />
            <div class="row mt-4" style="margin-bottom: 120px">
                <div class="col-md-4 mt-3">
                    <div class="card mx-auto" style="width: 22rem; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2)">
                        <img class="card-img-top" height="280px" src="https://asset.kompas.com/crops/W8xtCxoSCma22lYUkcOntWZRBjI=/0x0:1000x667/490x326/data/photo/2019/10/04/5d969cca82aef.jpg" alt="Card image cap" />
                        <div class="card-body">
                            <a href="#" id="neon1">Medis</a>
                            <h4 class="mt-2">INI JUDUL</h4>
                            <p class="card-text">
                                Some quick example text to build on the card title and make up
                                the bulk of the card's content.
                            </p>
                            <p>Author</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-3">
                    <div class="card mx-auto" style="width: 22rem; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2)">
                        <img class="card-img-top" height="280px;" src="https://realitarakyat.com/wp-content/uploads/2019/09/jantung.jpg" alt="Card image cap" />
                        <div class="card-body">
                            <a href="#" id="neon2">Medis</a>
                            <h4 class="mt-2">INI JUDUL</h4>
                            <p class="card-text">
                                Some quick example text to build on the card title and make up
                                the bulk of the card's content.
                            </p>
                            <p>Author</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 mt-3">
                    <div class="card mx-auto" style="width: 22rem; box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2)">
                        <img class="card-img-top" height="280px" src="https://cdn.akurat.co/images/uploads/images/akurat_20200805083825_hhiOMu.jpg" alt="Card image cap" />
                        <div class="card-body">
                            <a href="#" id="neon3">Medis</a>
                            <h4 class="mt-2">INI JUDUL</h4>
                            <p class="card-text">
                                Some quick example text to build on the card title and make up
                                the bulk of the card's content.
                            </p>
                            <p>Author</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row mb-4 mt-4" style="
            background-image: url('src/images/wave.svg');
            background-size: cover;
          ">
                <div class="col-md-6">
                    <p class="display-4 text-center mt-4" style="font-family: Libre Baskerville">
                        Good <strong style="color: #4cb7ff">Nutrition</strong> <br />
                        For <strong style="color: #4cb7ff">Healthy</strong> Life
                    </p>
                </div>
                <div class="col-md-6">
                    <div class="embed-responsive embed-responsive-16by9">
                        <iframe width="470" height="315" src="https://www.youtube.com/embed/c06dTj0v0sM" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
            <div class="row mt-5">
                <h2 class="mx-auto" style="font-family: Libre Baskerville">
                    Tentang Smart Healthy
                </h2>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-md-10">
                    <div class="card card-body">
                        <p>
                            <a class="fa fa-plus text-dark" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="text-decoration: none">
                                Apa itu Smart Healthy ?
                            </a>
                        </p>
                        <div class="collapse" id="collapseExample">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life
                            accusamus terry richardson ad squid. Nihil anim keffiyeh
                            helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-md-10">
                    <div class="card card-body">
                        <p>
                            <a class="fa fa-plus text-dark" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" style="text-decoration: none">
                                Apa saja manfaat dari Smart Healthy ?
                            </a>
                        </p>
                        <div class="collapse" id="collapseExample2">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life
                            accusamus terry richardson ad squid. Nihil anim keffiyeh
                            helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident.
                        </div>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center mt-3">
                <div class="col-md-10">
                    <div class="card card-body">
                        <p>
                            <a class="fa fa-plus text-dark" data-toggle="collapse" href="#collapseExample3" role="button" aria-expanded="false" aria-controls="collapseExample" style="text-decoration: none">
                                Fitur apa saja dari Smart Healthy yang bisa kamu gunakan ?
                            </a>
                        </p>
                        <div class="collapse" id="collapseExample3">
                            Anim pariatur cliche reprehenderit, enim eiusmod high life
                            accusamus terry richardson ad squid. Nihil anim keffiyeh
                            helvetica, craft beer labore wes anderson cred nesciunt sapiente
                            ea proident.
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>

    <!-- Footer -->
    <?= $this->load->view('./_partials/footer.php', "", TRUE) ?>
    <?= $this->load->view('./_partials/javascript.php', "", TRUE) ?>




</body>

</html>