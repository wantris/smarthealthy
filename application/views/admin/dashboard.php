<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Dashboard - SB Admin</title>

    <!-- Head Assets -->
    <?= $this->load->view('admin/_partialsAdmin/headAsset', "", true) ?>
</head>

<body class="sb-nav-fixed">

    <!-- Navbar -->
    <?= $this->load->view('admin/_partialsAdmin/navbar', "", true) ?>

    <div id="layoutSidenav">

        <!-- Sidebar -->
        <?= $this->load->view('admin/_partialsAdmin/sidebar', "", true) ?>

        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Dashboard</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Dashboard</li>
                    </ol>
                    <div class="row">
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-primary text-white mb-4">
                                <?php
                                $user = $this->db->query('SELECT * FROM user');

                                ?>
                                <div class="card-body">
                                    <div class="card-desc float-left">
                                        <p class="h1"><b><?= $user->num_rows(); ?></b></p>
                                        <p class="h4">User</p>
                                    </div>
                                    <i class="fas fa-user float-right" style="font-size: 70px;"></i>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="#"></a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-success text-white mb-4">
                                <?php
                                $artikel = $this->db->query('SELECT * FROM artikel');

                                ?>
                                <div class="card-body">
                                    <div class="card-desc float-left">
                                        <p class="h1"><b><?= $artikel->num_rows(); ?></b></p>
                                        <p class="h4">Artikel</p>
                                    </div>
                                    <i class="far fa-newspaper float-right" style="font-size: 70px;"></i>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="#">View Details</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-warning text-white mb-4">
                                <?php
                                $wait_verified = $this->db->query('SELECT * FROM artikel WHERE status="Wait Verified"');

                                ?>
                                <div class="card-body">
                                    <div class="card-desc float-left">
                                        <p class="h1"><b><?= $wait_verified->num_rows(); ?></b></p>
                                        <p class="h5">Artikel Pending</p>
                                    </div>
                                    <i class="fas fa-signature float-right" style="font-size: 50px;"></i>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="#">View Details</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-3 col-md-6">
                            <div class="card bg-info text-white mb-4">
                                <?php
                                $verified = $this->db->query('SELECT * FROM artikel WHERE status="Verified"');

                                ?>
                                <div class="card-body">
                                    <div class="card-desc float-left">
                                        <p class="h1"><b><?= $verified->num_rows(); ?></b></p>
                                        <p class="h5">Artikel Verified</p>
                                    </div>
                                    <i class="fas fa-check-circle float-right" style="font-size: 60px;"></i>
                                </div>
                                <div class="card-footer d-flex align-items-center justify-content-between">
                                    <a class="small text-white stretched-link" href="#">View Details</a>
                                    <div class="small text-white"><i class="fas fa-angle-right"></i></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xl-6">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-area mr-1"></i>
                                    Area Chart Example
                                </div>
                                <div class="card-body"><canvas id="myAreaChart" width="100%" height="40"></canvas></div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <div class="card mb-4">
                                <div class="card-header">
                                    <i class="fas fa-chart-bar mr-1"></i>
                                    Bar Chart Example
                                </div>
                                <div class="card-body"><canvas id="myBarChart" width="100%" height="40"></canvas></div>
                            </div>
                        </div>
                    </div>
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            DataTable Example
                        </div>
                        <div class="card-body">

                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url('assets/admin/js/scripts.js') ?>"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.8.0/Chart.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url('assets/admin/demo/chart-area-demo.js') ?>"></script>
    <script src="<?= base_url('assets/admin/demo/chart-bar-demo.js') ?>"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js" crossorigin="anonymous"></script>
    <script src="https://cdn.datatables.net/1.10.20/js/dataTables.bootstrap4.min.js" crossorigin="anonymous"></script>
    <script src="<?= base_url('assets/admin/demo/datatables-demo.js') ?>"></script>
</body>

</html>