<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data Artikel Penyakit</title>

    <!-- Head Assets -->
    <?= $this->load->view('admin/_partialsAdmin/headAsset', "", true) ?>
</head>

<body class="sb-nav-fixed">

    <!-- Navbar -->
    <?= $this->load->view('admin/_partialsAdmin/navbar', "", true) ?>

    <div id="layoutSidenav">

        <!-- Sidebar -->
        <?= $this->load->view('admin/_partialsAdmin/sidebar', "", true) ?>

        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Artikel Penyakit</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Update Data</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            Update Data Artikel Penyakit
                        </div>
                        <div class="card-body">
                            <?php
                            if ($this->session->flashdata('success') == true) {
                                echo '<div class="alert alert-success" role="alert">';
                                echo $this->session->flashdata('success');
                                echo '</div>';
                            } elseif ($this->session->flashdata('error') == true) {
                                echo '<div class="alert alert-danger" role="alert">';
                                echo $this->session->flashdata('error');
                                echo '</div>';
                            }
                            ?>
                            <?php foreach ($penyakit as $item) : ?>
                                <form method="POST" action="<?= site_url('admin/penyakit/updateData'); ?>">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Nama Penyakit</label>
                                        <input type="text" name="nama_penyakit" value="<?= $item->nama_penyakit ?>" class="form-control <?php echo form_error('nama_penyakit') ? 'is-invalid' : '' ?>" id="exampleInputEmail1" aria-describedby="emailHelp">
                                        <div class="invalid-feedback">
                                            <?php echo form_error('nama_penyakit') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Deskripsi Penyakit</label>
                                        <textarea name="deskripsi" class="deskripsi <?php echo form_error('deskripsi') ? 'is-invalid' : '' ?>" id="deskripsi"><?= $item->deskripsi ?></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('desktipsi') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Pencegahan Penyakit</label>
                                        <textarea name="pencegahan" class="deskripsi <?php echo form_error('pencegahan') ? 'is-invalid' : '' ?>" id="deskripsi"><?= $item->pencegahan ?></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('pencegahan') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Gejala Penyakit</label>
                                        <textarea name="gejala" class="deskripsi <?php echo form_error('gejala') ? 'is-invalid' : '' ?>" id="deskripsi"><?= $item->gejala ?></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('gejala') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Penyebab Penyakit</label>
                                        <textarea name="penyebab" class="deskripsi <?php echo form_error('penyebab') ? 'is-invalid' : '' ?>" id="deskripsi"><?= $item->penyebab ?></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('penyebab') ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Penanganan Penyakit</label>
                                        <textarea name="penanganan" class="deskripsi <?php echo form_error('penanganan') ? 'is-invalid' : '' ?>" id="deskripsi"><?= $item->penanganan ?></textarea>
                                        <div class="invalid-feedback">
                                            <?php echo form_error('penanganan') ?>
                                        </div>
                                    </div>
                                    <input type="hidden" name="id_penyakit" value="<?= $item->id_penyakit ?>">
                                    <input type="hidden" name="created_at" value="<?= $item->created_at ?>">
                                    <button type="submit" class="btn btn-primary">Submit</button>
                                </form>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>

    <!-- JS Assets -->
    <?= $this->load->view('admin/_partialsAdmin/jsAsset', "", true) ?>
</body>

</html>