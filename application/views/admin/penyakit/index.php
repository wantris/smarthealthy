<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data Artikel Penyakit</title>


    <!-- Head Assets -->
    <?= $this->load->view('admin/_partialsAdmin/headAsset', "", true) ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
    <link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" />
</head>

<body class="sb-nav-fixed">

    <!-- Navbar -->
    <?= $this->load->view('admin/_partialsAdmin/navbar', "", true) ?>

    <div id="layoutSidenav">

        <!-- Sidebar -->
        <?= $this->load->view('admin/_partialsAdmin/sidebar', "", true) ?>

        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Artikel Penyakit</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Data</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            Data Artikel Penyakit
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>Nama Penyakit</th>
                                            <th>Created At</th>
                                            <th>Update At</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>
                                    <?php $no = 1; ?>
                                    <tbody>
                                        <?php foreach ($penyakit->result() as $item) : ?>
                                            <tr id="<?php echo $item->id_penyakit; ?>">
                                                <td style="width: 40px;"><?= $no++ ?></td>
                                                <td><?= $item->nama_penyakit ?></td>
                                                <td style="width: 90px;"><?= $item->created_at ?></td>
                                                <td style="width: 90px;"><?= $item->updated_at ?></td>
                                                <td><a href="#" class="btn btn-success d-inline mr-2" id="detailPenyakit" data-toggle="modal" data-target=".detailPenyakit" data-nama-penyakit="<?= $item->nama_penyakit ?>" data-deskripsi="<?= $item->deskripsi ?>" data-pencegahan="<?= $item->pencegahan ?>" data-gejala="<?= $item->gejala ?>" data-penyebab="<?= $item->penyebab ?>" data-penanganan="<?= $item->penanganan ?>"><i class="fa fa-eye ml-1" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="<?= site_url('admin/penyakit/formUpdate/' . $item->id_penyakit) ?>" class="btn btn-primary d-inline mr-2"><i class="fa fa-pencil-square-o ml-1" aria-hidden="true"></i>
                                                    </a>
                                                    <a href="#" class="btn btn-danger d-inline remove" id="<?= $item->id_penyakit  ?>"><i class="fa fa fa-trash" aria-hidden="true"></i>

                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <div class="modal fade detailPenyakit" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content" id="modal-penyakit">

            </div>
        </div>
    </div>

    <!-- JS Assets -->
    <?= $this->load->view('admin/_partialsAdmin/jsAsset', "", true) ?>
    <script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script>
        $(document).on("click", "#detailPenyakit", function() {
            var namaPenyakit = $(this).data('nama-penyakit');
            var deskripsi = $(this).data('deskripsi');
            var pencegahan = $(this).data('pencegahan');
            var gejala = $(this).data('gejala');
            var penyebab = $(this).data('penyebab');
            var penanganan = $(this).data('penanganan');
            console.log(namaPenyakit + '=== deskripsi === ' + deskripsi + '=== pencegahan === ' + pencegahan + '=== gejala === ' + gejala + '=== penyebab === ' + penyebab + '=== penanganan === ' + penanganan);
            var viewPenyakit = '';
            viewPenyakit = `
            <div class="container-fluid">
                <div class="row">
                        <div class=col-md-12 col-sm-12>
                            <div id=judul class="text-center mt-3 mb-3 ">
                                <h3>${namaPenyakit}</h3>
                            </div>
                            <div id=konten class="px-5 py-2 text-secondary">
                                <p class="h5">Deskripsi</p>
                                <hr>
                                ${deskripsi}
                            </div>
                            <div id=konten class="px-5 py-2 text-secondary">
                                <p class="h5">Pencegahan</p>
                                <hr>
                                ${pencegahan}
                            </div>
                            <div id=konten class="px-5 py-2 text-secondary">
                                <p class="h5">gejala</p>
                                <hr>
                                ${gejala}
                            </div>
                            <div id=konten class="px-5 py-2 text-secondary">
                                <p class="h5">Penyebab</p>
                                <hr>
                                ${penyebab}
                            </div>
                            <div id=konten class="px-5 py-2 text-secondary">
                                <p class="h5">Penanganan</p>
                                <hr>
                                ${penanganan}
                            </div>

                        </div>
                </div>
            </div>
            `;

            document.getElementById("modal-penyakit").innerHTML = viewPenyakit;
        });
    </script>

    <script type="text/javascript">
        $(".remove").click(function() {
            var id = $(this).parents("tr").attr("id");
            console.log(id);

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '<?= site_url('admin/penyakit/delete'); ?>',
                            type: 'POST',
                            data: 'request=' + id,
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        });
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });

        });
    </script>
</body>

</html>