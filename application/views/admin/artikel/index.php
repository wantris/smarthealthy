<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <title>Data Artikel </title>


    <!-- Head Assets -->
    <?= $this->load->view('admin/_partialsAdmin/headAsset', "", true) ?>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>
    <link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" />
</head>

<body class="sb-nav-fixed" id="view-data">

    <!-- Navbar -->
    <?= $this->load->view('admin/_partialsAdmin/navbar', "", true) ?>

    <div id="layoutSidenav">

        <!-- Sidebar -->
        <?= $this->load->view('admin/_partialsAdmin/sidebar', "", true) ?>

        <div id="layoutSidenav_content">
            <main>
                <div class="container-fluid">
                    <h1 class="mt-4">Artikel Penyakit</h1>
                    <ol class="breadcrumb mb-4">
                        <li class="breadcrumb-item active">Data</li>
                    </ol>
                    <div class="card mb-4">
                        <div class="card-header">
                            <i class="fas fa-table mr-1"></i>
                            Data Artikel Penyakit
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="table-artikel" width="100%" cellspacing="0">
                                    <thead>
                                        <tr>
                                            <th>No</th>
                                            <th>User</th>
                                            <th>Judul</th>
                                            <th>Created At</th>
                                            <th>Status</th>
                                            <th>Aksi</th>
                                        </tr>
                                    </thead>

                                    <tbody>
                                        <?php $no = 1; ?>
                                        <?php foreach ($artikel->result() as $item) : ?>
                                            <tr id="<?php echo $item->artikel_id; ?>">
                                                <td style="width: 40px;"><?= $no++ ?></td>
                                                <td><?= $item->nama_depan . " " . $item->nama_belakang  ?></td>
                                                <td><?= $item->judul ?></td>
                                                <td><?= $item->tanggal ?></td>
                                                <td><?php if ($item->status === "Wait Verified") {  ?>
                                                        <button class="btn btn-primary"><?= $item->status ?></button>
                                                    <?php } elseif ($item->status === "Verified") { ?>
                                                        <button class="btn btn-success"><?= $item->status ?></button>
                                                    <?php } else { ?>
                                                        <button class="btn btn-danger"><?= $item->status ?></button>
                                                    <?php } ?>
                                                </td>

                                                <td><a href="#" class="btn btn-success d -inline mr-2" id="detailArtikel" data-toggle="modal" data-target=".detailArtikel" data-id="<?= $item->artikel_id  ?>" data-nama-user="<?= $item->nama_depan . " " . $item->nama_belakang ?>" data-judul="<?= $item->judul ?>" data-konten="<?= $item->konten ?>" data-tanggal="<?= $item->tanggal ?>" data-kategori="<?= $item->kategori ?>" data-status="<?= $item->status ?>" data-foto="<?= $item->foto ?>"><i class="fa fa-eye ml-1" aria-hidden="true"></i>
                                                    </a>

                                                    <a href="#" class="btn btn-danger d-inline remove" id="<?= $item->artikel_id  ?>"><i class="fa fa fa-trash" aria-hidden="true"></i>

                                                    </a>
                                                </td>
                                            </tr>
                                        <?php endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
            <footer class="py-4 bg-light mt-auto">
                <div class="container-fluid">
                    <div class="d-flex align-items-center justify-content-between small">
                        <div class="text-muted">Copyright &copy; Your Website 2020</div>
                        <div>
                            <a href="#">Privacy Policy</a>
                            &middot;
                            <a href="#">Terms &amp; Conditions</a>
                        </div>
                    </div>
                </div>
            </footer>
        </div>
    </div>
    <div class="modal fade detailArtikel" id="modalView" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" id="detail-view">
                </div>
                <div class="modal-footer" id="detail-footer">
                </div>
            </div>
        </div>
    </div>

    <!-- JS Assets -->
    <?= $this->load->view('admin/_partialsAdmin/jsAsset', "", true) ?>

    <script>
        $(document).ready(function() {
            $('#table-artikel').DataTable();
        });

        function convertDateTimeDBtoIndo(string) {
            bulanIndo = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

            date = string.split(" ")[0];
            time = string.split(" ")[1];

            tanggal = date.split("-")[2];
            bulan = date.split("-")[1];
            tahun = date.split("-")[0];

            return tanggal + " " + bulanIndo[Math.abs(bulan)] + " " + tahun + " " + time;
        }
        $(document).on("click", "#detailArtikel", function() {
            var id = $(this).data('id');
            var namaUser = $(this).data('nama-user');
            var judul = $(this).data('judul');
            var konten = $(this).data('konten');
            var tanggal = $(this).data('tanggal');
            var kategori = $(this).data('kategori');
            var status = $(this).data('status');
            var foto = $(this).data('foto');
            console.log(namaUser + '=== judul === ' + judul + '=== konten === ' + konten + '=== tanggal === ' + tanggal + '=== kategori === ' + kategori + '=== status === ' + status);
            var viewArtikel = '';

            viewArtikel = `
                <div class="row">
                    <div class=col-md-12 col-sm-12>
                    <img src="../../upload/poster/${foto}" class="img-fluid" width="760px">
                    </div
                </div>
                <div class="row">
                    <div class=col-md-12 col-sm-12>
                        <div id=judul class="text-center mt-3 ">
                            <h3>${judul}</h3>
                        </div>
                        <div id=konten class="px-5 py-2 text-secondary">
                            ${konten}
                        </div>
                        <div id="tanggal" class="px-5 mt-1 text-secondary">
                                
                        </div>
                        <div id="nama-user" class="px-5 mt-1 text-secondary">
                              ${namaUser}  
                        </div>
                    </div
                </div>
            `;
            var detailFooter = `
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            `;
            var detailFooter2 = `
                    <input type="submit" value="Verified" class="btn btn-success verified float-left" id="${id}" />
                    <input type="submit" value="Reject" class="btn btn-danger reject float-left mr-2" id="${id}" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            `;
            var detailFooter3 = `
                    <input type="submit" value="Verified" class="btn btn-success verified float-left" id="${id}" />
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            `;
            if (status === "Verified") {
                document.getElementById("detail-footer").innerHTML = detailFooter;
            } else if (status === "Unverified") {
                document.getElementById("detail-footer").innerHTML = detailFooter3;
            } else {
                document.getElementById("detail-footer").innerHTML = detailFooter2;
            }
            document.getElementById("detail-view").innerHTML = viewArtikel;
            document.getElementById("tanggal").innerHTML = convertDateTimeDBtoIndo(tanggal);

            //ajax Verified an aerticle
            $(".verified").click(function() {
                var id = $(this).attr("id");
                console.log(id);

                swal({
                        title: "Verified?",
                        text: "Are You Sure Want to Verified this Article?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-success",
                        cancelButtonClass: "btn-info",
                        confirmButtonText: "Yes, verified it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '<?= site_url('admin/artikel/verified'); ?>',
                                type: 'POST',
                                data: 'request=' + id,

                                success: function(data) {
                                    console.log(data);
                                    swal("Verified!", "Yeayy Article has been Verified", "success");
                                    setTimeout(function() { // wait for 5 secs(2)
                                        location.reload(); // then reload the page.(3)
                                    }, 5000);


                                },
                                error: function(error) {
                                    console.log(error);
                                    swal("Error", "", "error");
                                },
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    }
                );

            });

            //ajax Unverified an aerticle
            $(".reject").click(function() {
                var id = $(this).attr("id");
                console.log(id);

                swal({
                        title: "Reject?",
                        text: "Are You Sure Want to Reject this Article?",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonClass: "btn-danger",
                        cancelButtonClass: "btn-info",
                        confirmButtonText: "Yes, Reject it!",
                        cancelButtonText: "No, cancel it!",
                        closeOnConfirm: false,
                        closeOnCancel: false
                    },
                    function(isConfirm) {
                        if (isConfirm) {
                            $.ajax({
                                url: '<?= site_url('admin/artikel/unverified'); ?>',
                                type: 'POST',
                                data: 'request=' + id,

                                success: function(data) {
                                    console.log(data);
                                    swal("Reject!", "Article has been Unverified :(", "success");
                                    setTimeout(function() { // wait for 5 secs(2)
                                        location.reload(); // then reload the page.(3)
                                    }, 5000);


                                },
                                error: function(error) {
                                    console.log(error);
                                    swal("Error", "", "error");
                                },
                            });
                        } else {
                            swal("Cancelled", "", "error");
                        }
                    }
                );

            });
        });
    </script>

    <script type="text/javascript">
        $(".remove").click(function() {
            var id = $(this).parents("tr").attr("id");
            console.log(id);

            swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this imaginary file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonClass: "btn-danger",
                    confirmButtonText: "Yes, delete it!",
                    cancelButtonText: "No, cancel plx!",
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            url: '<?= site_url('admin/artikel/delete'); ?>',
                            type: 'POST',
                            data: 'request=' + id,
                            error: function() {
                                alert('Something is wrong');
                            },
                            success: function(data) {
                                $("#" + id).remove();
                                swal("Deleted!", "Your imaginary file has been deleted.", "success");
                            }
                        });
                    } else {
                        swal("Cancelled", "Your imaginary file is safe :)", "error");
                    }
                });

        });
    </script>
</body>

</html>