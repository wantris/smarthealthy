<nav class="navbar navbar-expand-lg navbar-light" id="navbar_top" style="
    box-shadow: 0 4px 2px -2px rgba(0, 0, 0, 0.2);
    background-color: white;
  ">
    <div class="container-fluid" style="margin-left: 0">
        <div class="col-sm-12 float-left">

            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <div class="collapse navbar-collapse float-left" id="navbarSupportedContent">
                <a class="navbar-brand mr-5 ml-3" href="#"><img src="<?= base_url('assets/icon/logo.svg') ?>" width="30" height="30" alt="" /></a>
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active mr-5">
                        <a class="nav-link" href="<?= site_url('') ?>">Home<span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item mr-5">
                        <a class="nav-link" href="<?= site_url('Artikel') ?>" id="cari">Artikel</a>
                    </li>
                    <li class="nav-item dropdown mr-5">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Info Kesehatan
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= site_url('portalSehat') ?>">Portal Kesehatan</a>
                            <a class="dropdown-item" href="<?= site_url('penyakit') ?>">Info Penyakit</a>
                            <a class="dropdown-item" href="#">Info Obat</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown mr-5">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Angka Kesehatan
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="<?= site_url('Kalkulatorsehat') ?>">Berat Badan Ideal</a>
                            <a class="dropdown-item" href="<?= site_url('Kalkulatorsehat/statistikcorona') ?>">Statistik Penyebaran Corona</a>
                        </div>
                    </li>
                </ul>

                <ul class="navbar-nav ml-4">
                    <?php if ($this->session->userdata('user_id') != null) { ?>
                        <li class="nav-item dropdown mr-4">
                            <a class="nav-link" data-toggle="dropdown" href="#">
                                <img src="<?= base_url('assets/image/icon.png') ?>" width="45px" alt="">
                            </a>
                            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right">
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo site_url('member/profil') ?>" class="dropdown-item">
                                    <i class="fa fa-wrench mr-2"></i>Settings
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?php echo site_url('admin/users/ubahPassword') ?>" class="dropdown-item">
                                    <i class="fa fa-key mr-2"></i>Ganti Password
                                </a>
                                <div class="dropdown-divider"></div>
                                <a href="<?= site_url('auth/logout') ?>" class="dropdown-item text-danger">
                                    <i class="fa fa-sign-out mr-2"></i>Log Out
                                </a>
                        </li>
                        <li class="nav-item mt-4" style="width: 140px;">
                            <a href="<?= site_url('member/tulisArtikel/' . $this->session->userdata('user_id')) ?>" style="text-decoration: none;font-size:17px" class="text-secondary "><img src="<?= base_url('assets/icon/search.svg')  ?>" class="mr-1" width="15%" alt="" />
                                Tulis Artikel
                            </a>
                        </li>

                    <?php } else { ?>
                        <li class="nav-item mt-1">
                            <a href="<?= site_url('Auth') ?>" style="text-decoration: none;" class="mr-5 text-secondary">Login</a>
                        </li>
                        <li class="nav-item mt-1" style="width:120px">
                            <a href="" style="text-decoration: none;" class="text-secondary;"><img src="<?= base_url('assets/icon/search.svg')  ?>" class="mr-1" width="8%" alt="" />
                                Tulis Artikel
                            </a>
                        </li>
                    <?php } ?>


                </ul>

            </div>
        </div>
    </div>

</nav>