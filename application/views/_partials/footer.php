<footer id="sticky-footer" class="py-4 text-white-50 mt-5" style="background-color: #1a9dff;">
    <div class="container text-center">
        <p class="text-white"><strong>Copyright &copy; Smart Healthy</strong></p>
    </div>
</footer>