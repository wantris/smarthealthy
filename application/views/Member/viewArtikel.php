<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.22/css/jquery.dataTables.css">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css">
    <script src="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.js"></script>

    <link rel="stylesheet" href="https://lipis.github.io/bootstrap-sweetalert/dist/sweetalert.css" />

</head>


<body style="background-color: #f2f5f7;" id="tampil">
    <?= $this->load->view('_partials/navbar', "", TRUE) ?>

    <div class="container mt-5 mb-5 px-4 py-4 bg-white">
        <div class="row">
            <div class="col sm-3">
                <div class="row mt-4 ">
                    <div class="col-12 ">
                        <a href="<?= site_url('Member/tulisArtikel') ?>" class="text-dark" style="text-decoration: none;">Tulis Artikel</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= site_url('member/profil') ?>" class="text-dark" style="text-decoration: none; color:black">Profile</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="#" id="linkProfile" class="text-primary" style="text-decoration: none; color:black">Artikel Saya</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="#" id="linkProfile" style="text-decoration: none; color:black">Password</a>
                    </div>
                </div>
                <hr class="text-secondary">
            </div>
            <div class="col-sm-9" id="artikel">
                <div class="row mt-4 ">
                    <div class="col-12">
                        <p class="h4 float-left pl-4" style="color: grey;">Artikel Kamu</p>

                        <a href="<?= site_url('Member/tulisArtikel') ?>" style="text-decoration: none;" class="btn-sm btn-primary float-right mt-2">Tulis Artikel</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-lg-4">
                        <select name="status" class="form-control" tabindex="-1" aria-hidden="true" id="status">
                            <option value="semua" <?php if ($status == "semua") {
                                                    ?> selected <?php } ?>>Semua Data</option>
                            <option value="Wait Verified" <?php if ($status == "belum") {
                                                            ?> selected <?php } ?>>Draft</option>
                            <option value="Verified" <?php if ($status == "sudah") {
                                                        ?> selected <?php } ?>>Disetujui</option>
                            <option value="Reject" <?php if ($status == "ditolak") {
                                                    ?> selected <?php } ?>>Ditolak</option>
                        </select>
                    </div>
                </div>
                <div class="row mt-3 text-secondary">
                    <div class="col-12">

                        <table id="artikelmu" style="width:100%;border:1px solid #eee;">
                            <thead>
                                <tr style="background-color: #ebf3fe;">
                                    <th style="width: 500px;" class="text-center">Judul</th>
                                    <th style="width: 320px;" class="text-center">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $this->db->select('*');
                                $this->db->from('artikel');
                                $this->db->where('artikel.user_id', $this->session->userdata("user_id"));
                                $this->db->join('user', 'user.user_id = artikel.user_id');
                                $query = $this->db->get()->row();
                                if (empty($query)) {
                                ?>
                                    <tr>
                                        <td colspan="2" class="text-center">Tidak Ada Data</td>
                                    </tr>
                                    <?php } else {
                                    foreach ($your as $item) :
                                    ?>
                                        <tr id="<?php echo $item->artikel_id; ?>">
                                            <td class="pl-2 pt-1 pb-1"><?= $item->judul ?></td>
                                            <td class="text-center pl-2 pt-2 pb-2"><a style="text-decoration: none;" href="#" class="btn-sm btn-danger remove"><i class="fa fa-trash ml-1" aria-hidden="true"></i>
                                                </a>
                                                <a style="text-decoration: none;" id="detail" href="#" data-toggle="modal" data-target="#modalView" class="btn-sm btn-success ml-2" data-id="<?= $item->artikel_id ?>" data-konten="<?= $item->konten ?>" data-foto="<?= $item->foto ?>" data-judul="<?= $item->judul ?>" data-tanggal="<?= $item->tanggal ?>"><i class="fa fa-eye ml-1" aria-hidden="true"></i>
                                                </a>
                                                <a style="text-decoration: none;" href="<?= site_url('member/updateArticle/' . $item->artikel_id) ?> " class="btn-sm btn-info ml-2"><i class="fa fa-pencil-square-o ml-1" aria-hidden="true"></i>
                                                </a>
                                            </td>
                                        </tr>

                                <?php endforeach;
                                } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Modal View-->
    <div class="modal fade" id="modalView" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-body" id="detail-view">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                </div>
            </div>
        </div>
    </div>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <?= $this->load->view('_partials/footer', "", TRUE) ?>

    <?= $this->load->view('_partials/javascript', "", TRUE) ?>

    <?= $this->load->view('member/partials', "", TRUE) ?>





</body>

</html>