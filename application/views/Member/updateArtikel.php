<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
</head>


<body style="background-color: #f2f5f7;">
    <?= $this->load->view('_partials/navbar', "", TRUE) ?>

    <div class="container mt-5 mb-5 px-4 py-4 bg-white" style="width: 770px;">
        <div class="row mb-2">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <a href="<?= site_url('member/viewArtikel') ?>" class="btn btn-secondary float-right"><i class="fa fa-trash" aria-hidden="true"></i></a>
            </div>
        </div>
        <form action="<?= site_url('member/updateProcess') ?>" method="post" enctype="multipart/form-data">
            <?php foreach ($artikel as $item) : ?>
                <div class="image">
                    <img src="<?= base_url('upload/poster/' . $item->foto) ?>" alt="<?= $item->foto ?>" style="width: 710px;" class="img-thumbnail img-fluid">
                </div>
                <div class="row mt-4 text-secondary">
                    <input type="hidden" name="artikel_id" value="<?= $item->artikel_id ?>">
                    <input type="hidden" name="user_id" value="<?= $item->user_id ?>">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1" class="font-weight-bold">PILIH KATEGORI</label>
                            <select class="form-control <?php echo form_error('kategori') ? 'is-invalid' : '' ?>" id=" kategori" name="kategori">
                                <option value="<?= $item->kategori ?>" selected><?= $item->kategori ?></option>
                                <option value="Alergi">Alergi</option>
                                <option value="Kanker">Kanker</option>
                                <option value="Mata">Mata</option>
                                <option value="Nutrisi">Nutrisi</option>
                                <option value="Olahraga">Olahraga</option>
                            </select>
                            <div class="invalid-feedback">
                                <?php echo form_error('kategori') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 text-secondary">
                    <div class="col-12">
                        <div class="form-group">
                            <label for="exampleFormControlSelect1" class="font-weight-bold">JUDUL</label>
                            <input type="text" class="form-control  <?php echo form_error('judul') ? 'is-invalid' : '' ?>" value="<?= $item->judul ?>" name="judul" placeholder="Input Judul...">
                            <div class="invalid-feedback">
                                <?php echo form_error('judul') ?>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 text-secondary">
                    <div class="col-12">
                        <label for="exampleFormControlSelect1" class="font-weight-bold">TULISAN</label>
                        <textarea id="editor1" name="konten" class="<?php echo form_error('konten') ? 'is-invalid' : '' ?>"><?= $item->konten ?></textarea>
                        <div class="invalid-feedback">
                            <?php echo form_error('konten') ?>
                        </div>
                    </div>
                </div>
                <div class="row mt-4 text-secondary">
                    <div class="col-12">
                        <label for="exampleFormControlSelect1" class="font-weight-bold">POSTER</label>
                        <input type="hidden" name="oldFoto" value="<?= $item->foto ?>" id="">
                        <input type="file" name="foto" class="form-control" id="">
                    </div>
                </div>
                <div class="row mt-4 text-secondary">
                    <div class="col-12">
                        <input type="submit" value="Update Artikel" class="btn btn-primary">
                    </div>
                </div>
            <?php endforeach; ?>
        </form>
        <input type="hidden" id="save_success" value="<?php echo $this->session->flashdata('update_message'); ?>">
    </div>
    </div>
    <script src="https://code.jquery.com/jquery-3.5.1.min.js" integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0=" crossorigin="anonymous"></script>
    <script src=" https://unpkg.com/sweetalert/dist/sweetalert.min.js"> </script>
    <script>
        if ($("#save_success").val() == 'success') {
            swal(

                {


                    title: "Yeayy Berhasil!",

                    text: "Artikel Berhasil Di Update",

                    icon: "success",

                    type: "success",

                }

            );
        } else if ($("#save_success").val() == 'failed') {
            swal(

                {


                    title: "Upss Gagal!",

                    text: "Artikel Gagal Di Update",

                    icon: "error",

                    type: "error",

                }

            );
        }
    </script>

    <script>
        CKEDITOR.replace('editor1');
    </script>
    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <?= $this->load->view('_partials/footer', "", TRUE) ?>
    <?= $this->load->view('_partials/javascript', "", TRUE) ?>

    <script>
        $(document).ready(function() {
            $('#kategori').select2();
        });
    </script>

</body>

</html>