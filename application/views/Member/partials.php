<script>
    function convertDateTimeDBtoIndo(string) {
        bulanIndo = ['', 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];

        date = string.split(" ")[0];
        time = string.split(" ")[1];

        tanggal = date.split("-")[2];
        bulan = date.split("-")[1];
        tahun = date.split("-")[0];

        return tanggal + " " + bulanIndo[Math.abs(bulan)] + " " + tahun + " " + time;
    }
    $(document).on("click", "#detail", function() {
        var articleId = $(this).data('id');
        var foto = $(this).data('foto');
        var judul = $(this).data('judul');
        var konten = $(this).data('konten');
        var tanggal = $(this).data('tanggal');
        var viewArticle = '';
        viewArticle = `
                <div class="row">
                    <div class=col-md-12 col-sm-12>
                    <img src="../../upload/poster/${foto}" class="img-fluid" width="760px">
                    </div
                </div>
                <div class="row">
                    <div class=col-md-12 col-sm-12>
                        <div id=judul class="text-center mt-3 ">
                            <h3>${judul}</h3>
                        </div>
                        <div id=konten class="px-5 py-2 text-secondary">
                            ${konten}
                        </div>
                        <div id="tanggal" class="px-5 mt-1 text-secondary">
                                
                        </div>
                    </div
                </div>
            `;
        document.getElementById("detail-view").innerHTML = viewArticle;
        document.getElementById("tanggal").innerHTML = convertDateTimeDBtoIndo(tanggal);
    });

    $('#modalView').modal('handleUpdate');
    $(document).ready(function() {
        $('#artikelmu').DataTable({
            "lengthChange": false,
            "bInfo": false,
            "bPaginate": false,

        });
        $("#status").on('change', function() {
            var value = $(this).val();

            $.ajax({
                url: "<?= site_url('member/getStatus'); ?>",
                type: 'POST',
                data: 'request=' + value,
                success: function(data) {
                    $("#tampil").html(data);
                },
            });
        });
    });

    $(".remove").click(function() {

        var id = $(this).parents("tr").attr("id");



        swal({

                title: "Apakah kamu yakin?",

                type: "warning",

                showCancelButton: true,

                confirmButtonClass: "ml-2 btn-danger",

                confirmButtonText: "Ya, Hapus!",

                cancelButtonText: "Tidak, Batalkan!",

                closeOnConfirm: false,

                closeOnCancel: false

            },

            function(isConfirm) {

                if (isConfirm) {

                    $.ajax({

                        url: '<?= site_url('member/deleteArticle'); ?>',
                        type: 'POST',
                        data: 'request=' + id,
                        error: function() {
                            alert('Something is wrong');
                        },
                        success: function(data) {
                            $("#" + id).remove();
                            swal("Deleted!", "Artikel Kamu Berhasil Dihapus", "success", 300);
                        }
                    });
                } else {

                    swal("Cancelled", "Your Article file is safe :)", "error");

                }

            });



    });
</script>