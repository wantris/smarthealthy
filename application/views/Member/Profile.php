<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
</head>


<body style="background-color: #f2f5f7;">
    <?= $this->load->view('_partials/navbar', "", TRUE) ?>

    <div class="container mt-5 mb-5 px-4 py-4 bg-white">
        <div class="row">
            <div class="col sm-3">
                <div class="row mt-4 ">
                    <div class="col-12 ">
                        <a href="<?= site_url('Member/tulisArtikel') ?>" class="text-dark" style="text-decoration: none;">Tulis Artikel</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="#" class="text-primary" style="text-decoration: none; color:black">Profile</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= site_url('Member/viewArtikel') ?>" id="linkProfile" style="text-decoration: none; color:black">Artikel Saya</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="#" id="linkProfile" style="text-decoration: none; color:black">Password</a>
                    </div>
                </div>
                <hr class="text-secondary">
            </div>
            <div class="col-sm-9" id="artikel">
                <div class="artikel">
                    <?php
                    if ($this->session->flashdata('profil') == true) {
                        echo '<div class="alert alert-success" role="alert">';
                        echo $this->session->flashdata('profil');
                        echo '</div>';
                    } elseif ($this->session->flashdata('errorProfile') == true) {
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $this->session->flashdata('errorProfile');
                        echo '</div>';
                    }
                    ?>
                    <form action="<?= site_url('member/changeProfil') ?>" method="POST" enctype="multipart/form-data">
                        <?php foreach ($user as $profil) : ?>
                            <div class="row mt-4 text-secondary">
                                <div class="col-12">
                                    <p class="h4 float-left pl-4">Profile</p>
                                    <button onclick="goBack()" class="btn btn-secondary float-right"><i class="fa fa-trash" aria-hidden="true"></i></button>
                                    <input type="submit" class="btn btn-primary float-right mr-4" value="Submit">
                                </div>
                            </div>
                            <hr class="text-secondary">
                            <div class="row mt-5 pl-4 pr-3 text-secondary">
                                <?php if ($profil->photo == null) { ?>
                                    <img src="<?= base_url('assets/image/icon.png') ?>" style="border-radius: 50%;" width="200px" alt="..." class="img-thumbnail">
                                <?php } elseif ($profil->photo != null) { ?>
                                    <img src="<?= base_url('upload/profil/' . $profil->photo) ?>" style="border-radius: 50%;" width="200px" alt="..." class="img-thumbnail">
                                <?php } ?>
                            </div>

                            <input type="hidden" name="id" value="<?= $profil->user_id ?>" id="">
                            <div class="row mt-4 pl-4 pr-3 text-secondary">
                                <div class="col-sm-6 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Nama Depan</label>
                                        <input type="text" class="form-control" name="nama_depan" value="<?= $profil->nama_depan ?>" placeholder="Enter Frontname" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-6 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Nama Belakang</label>
                                        <input type="text" class="form-control" name="nama_belakang" value="<?= $profil->nama_belakang ?>" placeholder="Enter Lastname" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4 pl-4 pr-3 text-secondary">
                                <div class="col-sm-12">
                                    <label for="exampleFormControlSelect1">Email</label>
                                    <input type="email" class="form-control" name="email" value="<?= $profil->email ?>" placeholder="Enter Email">
                                </div>
                            </div>
                            <div class="row mt-4 pl-4 pr-3 text-secondary">
                                <div class="col-sm-3 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Age</label>
                                        <input type="text" class="form-control" name="age" value="<?= $profil->age ?>" placeholder="Enter Age" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Weight</label>
                                        <input type="text" class="form-control" name="weight" value="<?= $profil->weight ?>" placeholder="Enter Weight" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Height</label>
                                        <input type="text" class="form-control" name="height" value="<?= $profil->height ?>" placeholder="Enter Height" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-3 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleFormControlSelect1">Jenis Kelamin</label>
                                        <select class="form-control" name="kelamin" id="exampleFormControlSelect1">
                                            <option value="<?= $profil->jenis_klm ?>" selected><?= $profil->jenis_klm ?></option>
                                            <option value="Laki-laki">Laki-Laki</option>
                                            <option value="Perempuan">Perempuan</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row mt-4 pl-4 pr-3 text-secondary">
                                <div class="col-sm-12">
                                    <label for="exampleFormControlSelect1">Profesi</label>
                                    <input type="text" class="form-control" name="profesi" value="<?= $profil->profesi ?>" placeholder="Enter Profesi">
                                </div>
                            </div>
                            <div class="row mt-4 pl-4 pr-3 text-secondary">
                                <div class="col-12">
                                    <label for="exampleFormControlSelect1" class="font-weight-bold">Foto</label>
                                    <input type="hidden" name="oldFoto" value="<?= $profil->photo ?>" id="">
                                    <input type="file" name="foto" class="form-control" id="">
                                </div>
                            </div>
                        <?php endforeach; ?>
                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>

    <script>
        function goBack() {
            window.history.back();
        }
    </script>
    <?= $this->load->view('_partials/footer', "", TRUE) ?>
    <?= $this->load->view('_partials/javascript', "", TRUE) ?>

    <script>
        // Replace the <textarea id="editor1"> with a CKEditor 4
        // instance, using default configuration.
        CKEDITOR.replace('#editor1', {
            customConfig: 'config.js'
        });

        $(document).ready(function() {
            $('#kategori').select2();
        });
    </script>

</body>

</html>