<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", TRUE) ?>
    <script src="https://cdn.ckeditor.com/4.15.0/standard/ckeditor.js"></script>
</head>


<body style="background-color: #f2f5f7;">
    <?= $this->load->view('_partials/navbar', "", TRUE) ?>

    <div class="container mt-5 mb-5 px-4 py-4 bg-white">
        <div class="row">
            <div class="col sm-3">
                <div class="row mt-4 ">
                    <div class="col-12 ">
                        <a href="#" class="text-primary" style="text-decoration: none;">Tulis Artikel</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= site_url('member/profil') ?>" id="linkProfile" style="text-decoration: none; color:black">Profile</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="<?= site_url('Member/viewArtikel') ?>" id="linkProfile" style="text-decoration: none; color:black">Artikel Saya</a>
                    </div>
                </div>
                <hr class="text-secondary">
                <div class="row">
                    <div class="col-12">
                        <a href="#" id="linkProfile" style="text-decoration: none; color:black">Password</a>
                    </div>
                </div>
            </div>
            <div class="col-sm-9" id="artikel">
                <div class="artikel">
                    <?php
                    if ($this->session->flashdata('success') == true) {
                        echo '<div class="alert alert-success" role="alert">';
                        echo $this->session->flashdata('success');
                        echo '</div>';
                    } elseif ($this->session->flashdata('error') == true) {
                        echo '<div class="alert alert-danger" role="alert">';
                        echo $this->session->flashdata('error');
                        echo '</div>';
                    }
                    ?>
                    <form action="<?= site_url('member/saveArtikel') ?>" method="POST" enctype="multipart/form-data">
                        <div class="row mt-4 text-secondary">
                            <div class="col-12">
                                <p class="h4 float-left">Tulis Artikel</p>
                                <a href="<?= site_url('member') ?>" class="btn btn-secondary float-right"><i class="fa fa-trash" aria-hidden="true"></i></a>
                                <input type="submit" class="btn btn-primary float-right mr-4" value="Submit">
                            </div>
                        </div>
                        <hr class="text-secondary">
                        <input type="hidden" name="user_id" value="<?= $this->session->userdata('user_id') ?>" id="">
                        <div class="row mt-4 text-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1" class="font-weight-bold">PILIH KATEGORI</label>
                                    <select class="form-control <?php echo form_error('kategori') ? 'is-invalid' : '' ?>" id=" kategori" name="kategori">
                                        <option selected>Pilih Kategori</option>
                                        <option value="Alergi">Alergi</option>
                                        <option value="Kanker">Kanker</option>
                                        <option value="Mata">Mata</option>
                                        <option value="Nutrisi">Nutrisi</option>
                                        <option value="Olahraga">Olahraga</option>
                                    </select>
                                    <div class="invalid-feedback">
                                        <?php echo form_error('kategori') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 text-secondary">
                            <div class="col-12">
                                <div class="form-group">
                                    <label for="exampleFormControlSelect1" class="font-weight-bold">JUDUL</label>
                                    <input type="text" class="form-control <?php echo form_error('judul') ? 'is-invalid' : '' ?>" name="judul" placeholder="Input Judul...">
                                    <div class="invalid-feedback">
                                        <?php echo form_error('judul') ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 text-secondary">
                            <div class="col-12">
                                <label for="exampleFormControlSelect1" class="font-weight-bold">TULISAN</label>
                                <textarea id="editor1" name="konten" class="<?php echo form_error('konten') ? 'is-invalid' : '' ?>"></textarea>
                                <div class="invalid-feedback">
                                    <?php echo form_error('konten') ?>
                                </div>
                            </div>
                        </div>
                        <div class="row mt-4 text-secondary">
                            <div class="col-12">
                                <label for="exampleFormControlSelect1" class="font-weight-bold">POSTER</label>
                                <input type="file" name="foto" class="form-control" id="">
                            </div>
                        </div>
                        <!--<div class="row mt-4  mb-5 text-secondary">
                    <div class="col-12">
                        <label for="exampleFormControlSelect1" class="font-weight-bold">UNGGAH FOTO</label>
                        <input type="file" class="form-control" id="inputGroupFile01" aria-describedby="inputGroupFileAddon01">
                        <small id="uploadHelp" class="form-text text-muted">Format JPG, GIF or PNG.</small>
                    </div>-->

                    </form>
                </div>
            </div>
        </div>
    </div>
    </div>


    <script>
        CKEDITOR.replace('editor1');
    </script>
    <?= $this->load->view('_partials/footer', "", TRUE) ?>
    <?= $this->load->view('_partials/javascript', "", TRUE) ?>

    <script>
        // Replace the <textarea id="editor1"> with a CKEditor 4
        // instance, using default configuration.
        CKEDITOR.replace('#editor1', {
            customConfig: 'config.js'
        });

        $(document).ready(function() {
            $('#kategori').select2();
        });
    </script>

</body>

</html>