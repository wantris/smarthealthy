<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true) ?>
</head>
<style>
    body {
        background-color: #f2f5f7;
    }
</style>

<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>
    <?php foreach ($user as $user) : ?>
        <div class="container-fluid">
            <div class="row mt-5">
                <div class="col-sm-4 mr-2 ">
                    <div class="row">
                        <div class="col-12">
                            <div class="card ml-5" style="width: 330px; height:660px">
                                <img src="<?= base_url('assets/image/icon.png') ?>" class="card-img-top rounded-circle mx-auto d-block mt-3" style="width: 190px;" alt="...">
                                <div class="card-body">
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <p>
                                                <a class="text-secondary font-weight-bold" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample" style="text-decoration: none">
                                                    PROFESI
                                                </a>
                                            </p>
                                            <div class="collapse text-primary" id="collapseExample">
                                                <?= $user->profesi ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row mt-3">
                                        <div class="col-12">
                                            <p>
                                                <a class="text-secondary font-weight-bold" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample" style="text-decoration: none">
                                                    JENIS KELAMIN
                                                </a>
                                            </p>
                                            <div class="collapse text-primary" id="collapseExample2">
                                                <?= $user->jenis_klm ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-sm-7">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <p class="h2" style="color: #1a9dff;"><?= $this->session->userdata('nama_depan');  ?> <?= $this->session->userdata('nama_belakang');  ?></p>
                                    <a href="<?= site_url('member/tulisArtikel/' . $this->session->userdata('user_id')) ?>" class="btn btn-outline-primary float-right mt-3"><span class="fa fa-pencil mr-2"></span>TULIS ARTIKEL</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row mt-3">
                        <div class="col-sm-12">
                            <div class="card">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-sm-3 text-center font-weight-bold text-secondary ">
                                            <p>MENULIS ARTIKEL</p>
                                            <p class="text-primary">
                                                <?php $id = $this->session->userdata('user_id');
                                                $query = $this->db->query("SELECT * FROM artikel where user_id = '$id'");
                                                echo $query->num_rows(); ?> Diterbitkan
                                            </p>

                                        </div>
                                        <div class="col-sm-9 text-center font-weight-bold ">
                                            <p class="text-secondary">DATA KESEHATAN</p>

                                            <div class="row text-primary">
                                                <div class="col-md-4 col-sm-4 text-center ">
                                                    <p>Age : <?= $user->age;  ?> th</p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 text-center">
                                                    <p>Weight : <?= $user->weight;  ?> kg</p>
                                                </div>
                                                <div class="col-md-4 col-sm-4 text-center">
                                                    <p>Height : <?= $user->height;  ?> cm</p>
                                                </div>
                                            </div>
                                        <?php endforeach; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <?= $this->load->view('_partials/footer', "", true) ?>
            <?= $this->load->view('_partials/javascript', "", true) ?>
</body>

</html>