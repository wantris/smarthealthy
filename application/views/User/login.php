<!DOCTYPE html>
<html lang="en">

<head>
    <?= $this->load->view('_partials/head', "", true) ?>
</head>

<body>
    <?= $this->load->view('_partials/navbar', "", true) ?>

    <div class="container forms">
        <?php
        if ($this->session->flashdata('success') == true) {
            echo '<div class="alert alert-success" role="alert">';
            echo $this->session->flashdata('success');
            echo '</div>';
        } elseif ($this->session->flashdata('errors') == true) {
            echo '<div class="alert alert-danger" role="alert">';
            echo $this->session->flashdata('errors');
            echo '</div>';
        }
        ?>
        <div class="row">
            <div class="col-sm-6 pr-4">
                <h2>Ayoo menjadi bagian dari <div class="text-primary">SmartHealthy</div>
                </h2>
                <p style="font-size: 20px;" class="mt-4">Apa manfaat yang anda dapat?</p>
                <hr>
                <p class="mt-4">Dengan bergabung di SmartHealthy, Anda secara eksklusif akan mendapatkan informasi terbaru dari kami. Anda juga dapat berbagi pengalaman lewat tulisan yang akan ditayangkan di website SmartHealthy, agar bisa menginspirasi banyak orang.</p>
            </div>
            <div class="col-sm-6">
                <div class="row">
                    <div id="lgn" class="col-sm-6 text-center px-3 py-3">
                        <a href="#" style="text-decoration: none; font-size:large; " id="login">Login</a>
                    </div>
                    <div id="lgn" class="col-sm-6 text-center px-3 py-3">
                        <a href="#" style="text-decoration: none; font-size:large " id="register">Daftar Baru</a>
                    </div>
                </div>
                <div class="row" id="viewLogin">
                    <div class="col-sm-12 pl-5 pr-5 pt-4 text-secondary">
                        <form action="<?= site_url('auth/login') ?>" method="POST">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <input type="username" class="form-control <?php echo form_error('username') ? 'is-invalid' : '' ?>" id="username1" name="username" autocomplete="off" aria-describedby="emailHelp" placeholder="Enter username">
                                <div class="invalid-feedback">
                                    <?php echo form_error('username') ?>
                                </div>
                            </div>
                            <div class="form-group mt-4">
                                <label for="exampleInputPassword1">Password</label>
                                <input type="password" class="form-control <?php echo form_error('password') ? 'is-invalid' : '' ?>" autocomplete="off" name="password" placeholder="Enter Password">
                                <div class="invalid-feedback">
                                    <?php echo form_error('password') ?>
                                </div>
                            </div>
                            <input type="submit" class="btn btn-primary mt-3 mb-5" value="Login" />
                        </form>
                        <hr>
                        <p>Belum memiliki akun? Daftar <a href="">Disini</a></p>
                    </div>
                </div>
                <div class="row" id="viewRegister">
                    <div class="col-sm-12 pl-5 pr-5 pt-4 text-secondary">
                        <form action="<?= site_url('auth/register') ?>" method="POST">
                            <div class="row">
                                <div class="col-sm-6 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Nama Depan</label>
                                        <input type="text" class="form-control" name="nama_depan" placeholder="Enter Frontname" autocomplete="off">
                                    </div>
                                </div>
                                <div class="col-sm-6 float-right">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Nama Belakang</label>
                                        <input type="text" class="form-control" name="nama_belakang" placeholder="Enter Lastname" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Email</label>
                                        <input type="email" class="form-control" name="email" placeholder="Enter email" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Username</label>
                                        <input type="username" name="username" class="form-control" placeholder="Enter username" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12 float-left">
                                    <div class="form-group mt-3">
                                        <label for="exampleInputEmail1">Password</label>
                                        <input type="password" name="password" class="form-control" placeholder="Enter password" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <button type="submit" class="btn btn-primary mt-3 mb-5">Register</button>
                        </form>
                        <hr>
                        <p>Sudah memiliki akun? Log in <a href="">Disini</a></p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <?= $this->load->view('_partials/footer', "", true) ?>

    <?= $this->load->view('_partials/javascript', "", true) ?>
    <script>
        $(document).ready(function() {
            $("#viewRegister").hide();
            $("#username1").val('');
            $("#register").click(function() {
                $("#viewLogin").hide('slow');
                $("#viewRegister").show('');
            });
            $("#login").click(function() {
                $("#viewRegister").hide('slow');
                $("#viewLogin").show('slow');
            });
        });
    </script>
</body>

</html>