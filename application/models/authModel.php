<?php
class authModel extends CI_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    function register($nama_depan, $nama_belakang, $email, $username, $password)
    {
        $data_user = array(
            'nama_depan' => $nama_depan,
            'nama_belakang' => $nama_belakang,
            'email' => $email,
            'username' => $username,
            'password' => MD5($password),

        );
        $this->db->insert('user', $data_user);
    }

    public function cek_login($username)
    {
        $hasil = $this->db->where('username', $username)->limit(1)->get('user');
        if ($hasil->num_rows() > 0) {
            return $hasil->row();
        } else {
            return array();
        }
    }


    public function isNotLogin()
    {
        return $this->session->userdata('user_logged') === null;
    }
}
