<?php
class modelPsehat extends CI_Model
{
    function get_alergi_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'ASC');
        $query = $this->db->get_where('artikel', array('kategori' => 'Alergi'), $limit, $start);
        return $query;
    }

    function get_kanker_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get_where('artikel', array('kategori' => 'Kanker'), $limit, $start);
        return $query;
    }

    function get_nutrisi_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get_where('artikel', array('kategori' => 'Nutrisi'), $limit, $start);
        return $query;
    }

    function get_mata_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get_where('artikel', array('kategori' => 'Mata'), $limit, $start);
        return $query;
    }

    function get_olahraga_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'DESC');
        $query = $this->db->get_where('artikel', array('kategori' => 'Olahraga'), $limit, $start);
        return $query;
    }

    function get_kecantikan_list($limit, $start)
    {
        $query = $this->db->get_where('artikel', array('kategori' => 'Kecantikan'), $limit, $start);
        return $query;
    }

    function get_pencernaan_list($limit, $start)
    {
        $query = $this->db->get_where('artikel', array('kategori' => 'Pencernaan'), $limit, $start);
        return $query;
    }

    function get_mental_list($limit, $start)
    {
        $query = $this->db->get_where('artikel', array('kategori' => 'Mental'), $limit, $start);
        return $query;
    }
}
