<?php
class penyakitModel extends CI_Model
{
    function get_penyakit_list()
    {
        $query = $this->db->get('penyakit ASC');
        return $query;
    }

    public function get_penyakit_keyword($keyword)
    {
        $this->db->select('*');
        $this->db->from('penyakit');
        $this->db->like('nama_penyakit', $keyword);
        return $this->db->get();
    }

    public function get_penyakit_ById($id)
    {
        $this->db->select('*');
        $this->db->from('penyakit');
        $this->db->where('id_penyakit', $id);
        return $this->db->get()->result();
    }
}
