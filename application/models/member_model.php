<?php
class member_model extends CI_Model
{
    private $_table = 'user';
    function getAllItem()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $this->session->userdata("user_id"));
        $query = $this->db->get()->result();
        return $query;
    }

    public function getById($user_id)
    {
        return $this->db->get_where($this->_table, ["user_id" => $user_id])->row();
    }

    public function getProfil()
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('user_id', $this->session->userdata("user_id"));
        $query = $this->db->get()->result();
        return $query;
    }

    public function updateProfile()
    {
        $user_id = $this->input->post('id');
        $np = $this->input->post('nama_depan');
        $nb = $this->input->post('nama_belakang');
        $email = $this->input->post('email');
        $age = $this->input->post('age');
        $weight = $this->input->post('weight');
        $height = $this->input->post('height');
        $profesi = $this->input->post('profesi');
        $jk = $this->input->post('kelamin');
        if (!empty($_FILES['foto']['name'])) {
            $foto = $this->_uploadFiles();
        } else {
            $foto = $this->input->post('oldFoto');
        }

        $data = array(
            'nama_depan' => $np,
            'nama_belakang' => $nb,
            'email' => $email,
            'age' => $age,
            'weight' => $weight,
            'height' => $height,
            'profesi' => $profesi,
            'jenis_klm' => $jk,
            'photo' => $foto
        );
        // var_dump($foto);
        $this->db->where('user_id', $user_id);
        $query = $this->db->update('user', $data);
        return $query;
    }

    private function _uploadFiles()
    {
        $config['upload_path']   = './upload/profil/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']         = 2048;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('foto')) {
            return $data = $this->upload->data("file_name");
        }
        // } else {
        //     $error = array('error' => $this->upload->display_errors());
        //     redirect('member/profil', $error);
        // }
    }

    public function getYourArtikel()
    {
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->where('artikel.user_id', $this->session->userdata("user_id"));
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $query = $this->db->get()->result();
        return $query;
    }

    public function getUnverified($status)
    {
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->where('artikel.status', $status);
        $this->db->where('artikel.user_id', $this->session->userdata("user_id"));
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $query = $this->db->get()->result();

        return $query;
    }

    public function getverified($status)
    {
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->where('artikel.status', $status);
        $this->db->where('artikel.user_id', $this->session->userdata("user_id"));
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $query = $this->db->get()->result();
        return $query;
    }
    public function getReject($status)
    {
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->where('artikel.status', $status);
        $this->db->where('artikel.user_id', $this->session->userdata("user_id"));
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $query = $this->db->get()->result();
        return $query;
    }

    public function getArticleById($id)
    {
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->where('artikel_id', $id);
        $query = $this->db->get()->result();
        return $query;
    }
}
