<?php
class m_penyakit extends CI_Model
{
    function dataPenyakit()
    {
        $query = $this->db->get('penyakit');
        return $query;
    }

    function getById($id_penyakit)
    {
        $this->db->select('*');
        $this->db->from('penyakit');
        $this->db->where('id_penyakit', $id_penyakit);
        $query = $this->db->get()->result();
        return $query;
    }

    function savePost()
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");
        $nama_penyakit = $this->input->post('nama_penyakit');
        $deskripsi = $this->input->post('deskripsi');
        $pencegahan = $this->input->post('pencegahan');
        $gejala = $this->input->post('gejala');
        $penyebab = $this->input->post('penyebab');
        $penanganan = $this->input->post('penanganan');

        $data_penyakit = array(
            'nama_penyakit' => $nama_penyakit,
            'deskripsi' => $deskripsi,
            'pencegahan' => $pencegahan,
            'gejala' => $gejala,
            'penyebab' => $penyebab,
            'penanganan' => $penanganan,
            'created_at'  => $now,
        );

        $this->db->insert('penyakit', $data_penyakit);
    }

    function updatePost()
    {

        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");
        $id_penyakit = $this->input->post('id_penyakit');
        $nama_penyakit = $this->input->post('nama_penyakit');
        $deskripsi = $this->input->post('deskripsi');
        $pencegahan = $this->input->post('pencegahan');
        $gejala = $this->input->post('gejala');
        $penyebab = $this->input->post('penyebab');
        $penanganan = $this->input->post('penanganan');
        $created_at = $this->input->post('created_at');

        $data_penyakit = array(
            'nama_penyakit' => $nama_penyakit,
            'deskripsi' => $deskripsi,
            'pencegahan' => $pencegahan,
            'gejala' => $gejala,
            'penyebab' => $penyebab,
            'penanganan' => $penanganan,
            'created_at'  => $created_at,
            'updated_at' => $now
        );
        $this->db->where('id_penyakit', $id_penyakit);
        $query = $this->db->update('penyakit', $data_penyakit);
        return $query;
    }
}
