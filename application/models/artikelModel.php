<?php
class artikelModel extends CI_Model
{

    function get_artikel_list($limit, $start)
    {
        $this->db->order_by('tanggal', 'DESC');
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $query = $this->db->get('artikel', $limit, $start);
        return $query;
    }

    function saveArtikel()
    {
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");
        $user_id = $this->input->post('user_id');
        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');
        $kategori = $this->input->post('kategori');

        $data_artikel = array(
            'user_id' => $user_id,
            'judul' => $judul,
            'konten' => $konten,
            'kategori' => $kategori,
            'status' => "Wait Verified",
            'tanggal' => $now,
            'foto'  => $this->_uploadFiles(),
        );

        $this->db->insert('artikel', $data_artikel);
    }

    private function _uploadFiles()
    {
        $config['upload_path']   = './upload/poster/';
        $config['allowed_types'] = 'jpg|png';
        $config['max_size']         = 2048;
        $this->load->library('upload', $config);
        if ($this->upload->do_upload('foto')) {
            return $this->upload->data("file_name");
        } else {
            $error = array('error' => $this->upload->display_errors());
            redirect('member/tulisArtikel', $error);
        }
    }

    public function getArtikel($judul)
    {
        $judul2 = str_replace("-", " ", $judul);
        $this->db->select('*');
        $this->db->from('artikel');
        $this->db->join('user', 'user.user_id = artikel.user_id');
        $this->db->where('artikel.judul', $judul2);
        $query = $this->db->get()->result();
        return $query;
    }

    public function updateArtikel()
    {
        date_default_timezone_set('Asia/Jakarta');
        $artikel_id = $this->input->post('artikel_id');
        $now = date("Y-m-d H:i:s");
        $user_id = $this->input->post('user_id');
        $judul = $this->input->post('judul');
        $konten = $this->input->post('konten');
        $kategori = $this->input->post('kategori');
        if (!empty($_FILES['foto']['name'])) {
            $foto = $this->_uploadFiles();
        } else {
            $foto = $this->input->post('oldFoto');
        }

        $data_artikel = array(
            'user_id' => $user_id,
            'judul' => $judul,
            'konten' => $konten,
            'kategori' => $kategori,
            'status' => "Wait Verified",
            'tanggal' => $now,
            'foto'  => $foto,
        );
        // var_dump($foto);
        $this->db->where('artikel_id', $artikel_id);
        $query = $this->db->update('artikel', $data_artikel);
        return $query;
    }
}
