<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('artikelModel');
        $this->load->model('authModel');
        if ($this->authModel->isNotLogin()) {
            echo '<script>alert("Anda Belum Login");window.location.href="' . site_url('auth') . '";</script>';
        }
    }

    public function index()
    {
        $this->load->view('user/profile');
    }
}
