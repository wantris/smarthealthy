<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Penyakit extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->library('pagination');

        $this->load->model('penyakitModel');
    }

    public function index()
    {
        $data['penyakit'] = $this->penyakitModel->get_penyakit_list();
        $this->load->view('Penyakit/listPenyakit', $data);
    }

    public function search()
    {
        $keyword = $this->input->get('keyword');
        $data['penyakit'] = $this->penyakitModel->get_penyakit_keyword($keyword);
        $this->load->view('penyakit/listPenyakit', $data);
    }

    public function getPenyakit($id)
    {
        $data['penyakit'] = $this->penyakitModel->get_penyakit_ById($id);
        // var_dump($data);
        $this->load->view('penyakit/getPenyakit', $data);
    }
}
