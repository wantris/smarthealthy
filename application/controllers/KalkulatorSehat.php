<?php
defined('BASEPATH') or exit('No direct script access allowed');

class KalkulatorSehat extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
    }

    public function index()
    {
        $this->load->view('kalkulatorsehat/bbi.php');
    }

    public function hasilbbi()
    {
        $tinggi = $this->input->post('tinggi') / 100;
        $berat = $this->input->post('berat');

        $hasil = $berat / ($tinggi * $tinggi);

        $data = array(
            'hasil' => $hasil,
            'tinggi' => $tinggi,
            'berat' => $berat

        );
        $this->load->view('kalkulatorsehat/hasilbbi.php', $data);
    }


    public function kaloriharian()
    {
        $this->load->view('kalkulatorsehat/kaloriHarian.php');
    }

    public function statistikcorona()
    {

        $province = file_get_contents("https://api.kawalcorona.com/indonesia/provinsi");

        $data["provinsi"] = json_decode($province, true);
        $data["coba"] = file_get_contents('https://api.kawalcorona.com/indonesia');


        $this->load->view('kalkulatorsehat/statistikcorona.php', $data); //




        // foreach ($obj as $obj) {
        //     print_r($obj["attributes"]["Provinsi"]);
        // }
    }
}
