<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Member extends CI_Controller
{


    function __construct()
    {
        parent::__construct();
        $this->load->model('member_model');
        $this->load->model('artikelModel');
        $this->load->model('authModel');
        if ($this->authModel->isNotLogin()) {
            echo '<script>alert("Anda Belum Login");window.location.href="' . site_url('auth') . '";</script>';
        }
    }

    public function back()
    {
        redirect($_SERVER['HTTP_REFERER']);
    }

    public function index()
    {
        $data['user'] = $this->member_model->getAllItem();
        $this->load->view('member/member', $data);
    }

    public function tulisArtikel()
    {

        $this->load->view('member/tulisArtikel');
    }

    public function saveArtikel()
    {
        $this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'Konten', 'trim|required|min_length[10]');
        $artikel = $this->artikelModel;
        $member = $this->member_model;

        if ($this->form_validation->run() == true) {
            $artikel->saveArtikel();
            $this->session->set_flashdata('success', 'Berhasil menambahkan artikel');
            $this->tulisArtikel();
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('member/tulisArtikel');
        }
    }

    public function Profil()
    {
        $data['user'] = $this->member_model->getProfil();
        $this->load->view('member/profile', $data);
    }

    public function changeProfil()
    {
        $member = $this->member_model;
        $this->form_validation->set_rules('nama_depan', 'Nama depan', 'trim|required');
        $this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('age', 'Age', 'trim|required');
        $this->form_validation->set_rules('weight', 'Weight', 'trim|required');
        $this->form_validation->set_rules('height', 'Height', 'trim|required');
        $this->form_validation->set_rules('profesi', 'Profesi', 'trim|required');

        if ($this->form_validation->run() == true) {
            $member->updateProfile();
            $this->session->set_flashdata('profil', 'Berhasil Update Profil');
            $this->Profil();
        } else {
            $this->session->set_flashdata('errorProfile', validation_errors());
            $this->Profil();
        }
    }

    public function viewArtikel()
    {
        $data['your'] = $this->member_model->getYourArtikel();
        $data["status"] = "semua";
        $this->load->view('member/viewArtikel', $data);
    }

    public function getStatus()
    {
        $status = $this->input->post('request');
        if ($status == "Wait Verified") {
            $data['your'] = $this->member_model->getUnverified($status);
            $data["status"] = "belum";
            $this->load->view('member/viewArtikel', $data);
        } elseif ($status == "Verified") {
            $data['your'] = $this->member_model->getVerified($status);
            $data["status"] = "sudah";
            $this->load->view('member/viewArtikel', $data);
        } elseif ($status == "Reject") {
            $data['your'] = $this->member_model->getReject($status);
            $data["status"] = "ditolak";
            $this->load->view('member/viewArtikel', $data);
        } else {
            $data["status"] = "semua";
            $data['your'] = $this->member_model->getYourArtikel();
            $this->load->view('member/viewArtikel', $data);
        }
    }

    public function updateArticle($id)
    {
        $data['artikel'] = $this->member_model->getArticleById($id);

        $this->load->view('member/updateArtikel', $data);
    }

    public function updateProcess()
    {
        $id = $this->input->post('artikel_id');
        $this->form_validation->set_rules('kategori', 'Kategori', 'trim|required');
        $this->form_validation->set_rules('judul', 'Judul', 'trim|required');
        $this->form_validation->set_rules('konten', 'Konten', 'trim|required|min_length[10]');
        $artikel = $this->artikelModel;
        $member = $this->member_model;

        if ($this->form_validation->run() == true) {
            $artikel->updateArtikel();
            $this->session->set_flashdata('update_message', 'success');
            $this->updateArticle($id);
        } else {
            $this->session->set_flashdata('update_message', 'failed');
            $this->updateArticle($id);
        }
    }

    public function deleteArticle()
    {
        $id = $this->input->post('request');
        $this->db->delete('artikel', array('artikel_id' => $id));
        $this->viewArtikel();
    }
}
