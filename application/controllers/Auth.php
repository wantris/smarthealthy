<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('authModel');
    }

    public function index()
    {
        $this->load->view('User/login');
    }

    public function login()
    {
        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');

        if ($this->form_validation->run() == FALSE) {
            $this->session->set_flashdata('errors', validation_errors());
            redirect('auth');
        } elseif ($this->form_validation->run() == true) {
            $username = $this->input->post('username');
            $pass = $this->input->post('password');
            $cek_login = $this->authModel->cek_login($username);

            if ($cek_login == FALSE) {
                echo '<script>alert("Username yang Anda masukan salah.");window.location.href="' . site_url('auth') . '";</script>';
            } else {
                if (MD5($pass) == $cek_login->password) {
                    $data_session = array(
                        'user_id' => $cek_login->user_id,
                        'nama_depan' => $cek_login->nama_depan,
                        'nama_belakang' => $cek_login->nama_belakang,
                    );
                    $this->session->set_userdata(['user_logged' => $data_session]);
                    $this->session->set_userdata($data_session);
                    echo '<script>alert("Selamaat Anda Berhasil Login");window.location.href="' . site_url('member') . '";</script>';
                } else {
                    $this->session->set_flashdata('errors', 'Password Salah');
                    redirect('auth');
                }
            }
        }
    }

    public function register()
    {
        $this->form_validation->set_rules('nama_depan', 'Nama Depan', 'trim|required');
        $this->form_validation->set_rules('nama_belakang', 'Nama Belakang', 'trim|required');
        $this->form_validation->set_rules('email', 'Email', 'trim|required');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|is_unique[user.username]');
        $this->form_validation->set_rules('password', 'Password', 'trim|required');

        if ($this->form_validation->run() == true) {
            $nama_depan = $this->input->post('nama_depan');
            $nama_belakang = $this->input->post('nama_belakang');
            $email = $this->input->post('email');
            $username = $this->input->post('username');
            $password = $this->input->post('password');

            $this->authModel->register($nama_depan, $nama_belakang, $email, $username, $password);
            $this->session->set_flashdata('success', 'Proses Pendaftaran Berhasil');
            redirect('auth');
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('auth');
        }
    }

    public function viewBerhasil()
    {
        $this->load->view('user/viewBerhasil');
    }


    public function logout()
    {
        $this->session->sess_destroy();
        echo '<script>alert("Anda berhasil Log Out.");window.location.href="' . site_url('auth') . '";</script>';
    }
}
