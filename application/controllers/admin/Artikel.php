<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Artikel extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('m_artikel');
    }

    public function index()
    {
        $data['artikel'] = $this->m_artikel->dataArtikel();
        $this->load->view('admin/artikel/index.php', $data);
    }

    public function dataVerified()
    {
        $data['artikel'] = $this->m_artikel->dataVerified();
        $this->load->view('admin/artikel/verified.php', $data);
    }
    public function dataWait()
    {
        $data['artikel'] = $this->m_artikel->dataWait();
        $this->load->view('admin/artikel/waitVerified.php', $data);
    }
    public function dataUnverified()
    {
        $data['artikel'] = $this->m_artikel->dataUnverified();
        $this->load->view('admin/artikel/unverified.php', $data);
    }

    public function formTambah()
    {
        $this->load->view('admin/penyakit/addPenyakit.php');
    }

    public function postData()
    {
        $this->form_validation->set_rules('nama_penyakit', 'Nama Penyakit', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskipsi Penyakit', 'trim|required');
        $this->form_validation->set_rules('pencegahan', 'Pencegahan Penyakit', 'trim|required');
        $this->form_validation->set_rules('gejala', 'Gejala Penyakit', 'trim|required');
        $this->form_validation->set_rules('penyebab', 'Penyebab Penyakit', 'trim|required');
        $this->form_validation->set_rules('penanganan', 'Penanganan Penyakit', 'trim|required');

        $penyakit = $this->m_penyakit;

        if ($this->form_validation->run() == true) {
            $penyakit->savePost();
            $this->session->set_flashdata('success', 'Berhasil menambahkan artikel');
            $this->formTambah();
        } else {
            $this->session->set_flashdata('error', validation_errors());
            redirect('admin/Penyakit/formTambah');
        }
    }

    public function formUpdate($id_penyakit)
    {
        $data['penyakit'] = $this->m_penyakit->getById($id_penyakit);
        $this->load->view('admin/penyakit/updatePenyakit.php', $data);
    }

    public function updateData()
    {
        $this->form_validation->set_rules('nama_penyakit', 'Nama Penyakit', 'trim|required');
        $this->form_validation->set_rules('deskripsi', 'Deskipsi Penyakit', 'trim|required');
        $this->form_validation->set_rules('pencegahan', 'Pencegahan Penyakit', 'trim|required');
        $this->form_validation->set_rules('gejala', 'Gejala Penyakit', 'trim|required');
        $this->form_validation->set_rules('penyebab', 'Penyebab Penyakit', 'trim|required');
        $this->form_validation->set_rules('penanganan', 'Penanganan Penyakit', 'trim|required');

        $penyakit = $this->m_penyakit;

        if ($this->form_validation->run() == true) {
            $penyakit->updatePost();
            $this->session->set_flashdata('success', 'Berhasil Update Artikel');
            $this->formUpdate($this->input->post('id_penyakit'));
        } else {
            $this->session->set_flashdata('error', validation_errors());
            $this->formUpdate($this->input->post('id_penyakit'));
        }
    }

    public function verified()
    {
        $id = $this->input->post('request');
        $this->db->set('status', 'Verified');
        $this->db->where('artikel_id', $id);
        $this->db->update('artikel');
    }

    public function unverified()
    {
        $id = $this->input->post('request');
        $this->db->set('status', 'Unverified');
        $this->db->where('artikel_id', $id);
        $this->db->update('artikel');

        $this->index();
    }

    public function delete()
    {
        $id = $this->input->post('request');
        $this->db->delete('artikel', array('artikel_id' => $id));
    }
}
